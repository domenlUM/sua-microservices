package grpcMiddleware

import (
	"context"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/google/uuid"
	"github.com/streadway/amqp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func CorrelationIdLogger(amqpChannel *amqp.Channel, queue *amqp.Queue, serviceName string) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, serverInfo *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		var correlationId string
		md, ok := metadata.FromIncomingContext(ctx)
		if ok {
			correlationId = md.Get("correlationId")[0]
		}

		err := amqpChannel.Publish(
			"",         // exchange
			queue.Name, // routing key
			false,      // mandatory
			false,      // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(time.Now().String() + " INFO " + serviceName + " " + serverInfo.FullMethod + " CORRELATION:" + correlationId),
			})
		if err != nil {
			sentry.CaptureException(err)
			return nil, err
		}

		return handler(ctx, req)
	}
}

func AddCorrelationFromContext() grpc.UnaryClientInterceptor {
	return func(
		ctx context.Context,
		method string,
		req interface{},
		reply interface{},
		cc *grpc.ClientConn,
		invoker grpc.UnaryInvoker,
		opts ...grpc.CallOption,

	) error {
		correlationId, ok := ctx.Value("correlationId").(string)
		if !ok {
			correlationId = uuid.New().String()
		}

		ctx = metadata.AppendToOutgoingContext(ctx, "correlationId", correlationId)

		return invoker(ctx, method, req, reply, cc, opts...)
	}
}
