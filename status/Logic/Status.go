package Logic

import (
	"context"

	"golang.org/x/sync/errgroup"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
)

func (c *Controller) PingAll(ctx context.Context) (statuses []DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "PingAll", "Logic")
	defer span.End()

	statuses = make([]DataStructures.Status, 5)

	errs, ctx := errgroup.WithContext(ctx)

	errs.Go(func() error {
		status, err1 := c.authAPI.Ping(ctx)
		if err1 != nil {
			return err1
		}
		statuses[0] = status
		return nil
	})
	errs.Go(func() error {
		status, err1 := c.serviceAPI.Ping(ctx)
		if err1 != nil {
			return err1
		}
		statuses[1] = status
		return nil
	})
	errs.Go(func() error {
		status, err1 := c.supplyAPI.Ping(ctx)
		if err1 != nil {
			return err1
		}
		statuses[2] = status
		return nil
	})
	errs.Go(func() error {
		status, err1 := c.ticketingAPI.Ping(ctx)
		if err1 != nil {
			return err1
		}
		statuses[3] = status
		return nil
	})
	errs.Go(func() error {
		status, err1 := c.paymentsAPI.Ping(ctx)
		if err1 != nil {
			return err1
		}
		statuses[4] = status
		return nil
	})

	err = errs.Wait()

	return statuses, err
}
