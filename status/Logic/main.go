package Logic

import (
	"gitlab.com/domenlUM/sua-microservices/clients/AuthenticationAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/PaymentsAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/ServiceAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/SupplyAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/TicketingAPI"
)

type Controller struct {
	authAPI      *AuthenticationAPI.Client
	paymentsAPI  *PaymentsAPI.Client
	serviceAPI   *ServiceAPI.Client
	supplyAPI    *SupplyAPI.Client
	ticketingAPI *TicketingAPI.Client
}

func New(authClient *AuthenticationAPI.Client, client2 *PaymentsAPI.Client, client3 *ServiceAPI.Client, client4 *SupplyAPI.Client, client5 *TicketingAPI.Client) *Controller {
	return &Controller{
		authAPI:      authClient,
		paymentsAPI:  client2,
		serviceAPI:   client3,
		supplyAPI:    client4,
		ticketingAPI: client5,
	}
}
