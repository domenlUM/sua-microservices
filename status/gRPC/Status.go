package gRPC

import (
	"context"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Status_gRPC"
)

func (s *gRPCServer) Ping(_ context.Context, _ *Common_gRPC.Empty) (*Common_gRPC.Status, error) {
	return &Common_gRPC.Status{Online: true, Title: "Status"}, nil
}

func (s *gRPCServer) PingAll(ctx context.Context, _ *Common_gRPC.Empty) (*Status_gRPC.Statuses, error) {
	statuses, err := s.c.PingAll(ctx)
	if err != nil {
		sentry.CaptureException(err)
		return nil, err
	}

	statusesgrpc := make([]*Common_gRPC.Status, len(statuses))
	for i := range statuses {
		statusesgrpc[i] = &Common_gRPC.Status{
			Online: statuses[i].Status,
			Title:  statuses[i].Service,
		}
	}

	return &Status_gRPC.Statuses{Statuses: statusesgrpc}, err
}
