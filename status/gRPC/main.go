package gRPC

import (
	"fmt"
	"net"
	"net/http"

	"github.com/streadway/amqp"

	"gitlab.com/domenlUM/sua-microservices/grpcMiddleware"

	"github.com/getsentry/sentry-go"
	"github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/labstack/gommon/log"
	"gitlab.com/domenlUM/sua-microservices/Generated/Status_gRPC"
	"gitlab.com/domenlUM/sua-microservices/status/Logic"
	"go.elastic.co/apm/module/apmgrpc"
	"google.golang.org/grpc"
)

type gRPCServer struct {
	c *Logic.Controller
	Status_gRPC.UnimplementedStatusAPIServer
}

func New(c *Logic.Controller) *gRPCServer {
	return &gRPCServer{c: c}
}

func (s *gRPCServer) Start(stopChannel chan bool, amqpCh *amqp.Channel, queue *amqp.Queue) {
	options := []grpc.ServerOption{
		grpc.ChainUnaryInterceptor(grpc_recovery.UnaryServerInterceptor(), apmgrpc.NewUnaryServerInterceptor(), grpcMiddleware.CorrelationIdLogger(amqpCh, queue, "STATUS")),
		grpc.ChainStreamInterceptor(grpc_recovery.StreamServerInterceptor(), apmgrpc.NewStreamServerInterceptor()),
	}

	grpcServer := grpc.NewServer(options...)

	go func(stopChannel chan bool) {
		<-stopChannel

		grpcServer.GracefulStop()
	}(stopChannel)

	go func() {
		lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8001))
		if err != nil {
			log.Error("server listen on port %d error:%v", 8001, err)
			return
		}

		Status_gRPC.RegisterStatusAPIServer(grpcServer, s)
		fmt.Println("Starting gRPC")
		if err := grpcServer.Serve(lis); err != nil && err != http.ErrServerClosed {
			sentry.CaptureException(err)
		}
	}()
}
