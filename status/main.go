package main

import (
	"context"
	"fmt"
	"os"

	"github.com/streadway/amqp"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/clients/AuthenticationAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/PaymentsAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/ServiceAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/SupplyAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/TicketingAPI"
	"gitlab.com/domenlUM/sua-microservices/status/Logic"
	"gitlab.com/domenlUM/sua-microservices/status/gRPC"
)

func main() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://a329486ee78e4b5aa8133e82fe7a360d@o1035781.ingest.sentry.io/6147811",
	})
	if err != nil {
		fmt.Printf("sentry.Init: %s\n", err)
	}

	ctx := context.Background()

	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		sentry.CaptureException(err)
	}

	ch, err := conn.Channel()
	if err != nil {
		sentry.CaptureException(err)
	}

	q, err := ch.QueueDeclare(
		"logging", // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		sentry.CaptureException(err)
	}

	authAPI := AuthenticationAPI.New()
	err = authAPI.Init(ctx, getEnv("AUTHENTICATION_API_GRPC_ADDRESS", "authentication:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	paymentsAPI := PaymentsAPI.New()
	err = paymentsAPI.Init(ctx, getEnv("PAYMENTS_API_GRPC_ADDRESS", "payments:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	serviceAPI := ServiceAPI.New()
	err = serviceAPI.Init(ctx, getEnv("SERVICE_API_GRPC_ADDRESS", "service:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	supplyAPI := SupplyAPI.New()
	err = supplyAPI.Init(ctx, getEnv("SUPPLY_API_GRPC_ADDRESS", "supply:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	ticketingAPI := TicketingAPI.New()
	err = ticketingAPI.Init(ctx, getEnv("TICKETING_API_GRPC_ADDRESS", "ticketing:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	logicController := Logic.New(authAPI, paymentsAPI, serviceAPI, supplyAPI, ticketingAPI)
	if err != nil {
		sentry.CaptureException(err)
		fmt.Println(err.Error())
		return
	}

	grpcAPI := gRPC.New(logicController)

	signalChan := make(chan os.Signal, 0)
	closeGRPC := make(chan bool, 0)

	grpcAPI.Start(closeGRPC, ch, &q)

	<-signalChan

	close(closeGRPC)
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
