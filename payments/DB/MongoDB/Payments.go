package MongoDB

import (
	"context"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (db *MongoDB) GetPayments(ctx context.Context) (payments []DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "GetPayments", "MongoDB")
	defer span.End()

	opts := options.Find()

	cursor, err := db.client.Database(db.database).Collection("payments").Find(ctx, bson.M{}, opts)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	payments = make([]DataStructures.Payment, 0)

	err = cursor.All(ctx, &payments)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) GetPaymentById(ctx context.Context, paymentID primitive.ObjectID) (payment DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "GetPaymentById", "MongoDB")
	defer span.End()

	err = db.client.Database(db.database).Collection("payments").FindOne(ctx, bson.M{"_id": paymentID}).Decode(&payment)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) OpenPayment(ctx context.Context, payment DataStructures.Payment) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "OpenPayment", "MongoDB")
	defer span.End()

	result, err := db.client.Database(db.database).Collection("payments").InsertOne(ctx, payment)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return result.InsertedID.(primitive.ObjectID), nil
}

func (db *MongoDB) ClosePayment(ctx context.Context, paymentID primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "ClosePayment", "MongoDB")
	defer span.End()

	_, err = db.client.Database(db.database).Collection("payments").UpdateOne(ctx, bson.M{"_id": paymentID}, bson.M{
		"$set": bson.M{"payment_state": 2},
	})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}
