package DB

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DB interface {
	Init(ctx context.Context) (err error)
	Close(ctx context.Context) (err error)
	GetPaymentById(ctx context.Context, paymentID primitive.ObjectID) (payment DataStructures.Payment, err error)
	GetPayments(ctx context.Context) (payments []DataStructures.Payment, err error)
	OpenPayment(ctx context.Context, payment DataStructures.Payment) (id primitive.ObjectID, err error)
	ClosePayment(ctx context.Context, paymentID primitive.ObjectID) (err error)
}
