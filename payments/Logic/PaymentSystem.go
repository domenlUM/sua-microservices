package Logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c *Controller) GetPayments(ctx context.Context) (payments []DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "GetPayments", "Logic")
	defer span.End()

	return c.db.GetPayments(ctx)
}

func (c *Controller) GetPaymentById(ctx context.Context, paymentID primitive.ObjectID) (payment DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "GetPaymentById", "Logic")
	defer span.End()

	return c.db.GetPaymentById(ctx, paymentID)
}

func (c *Controller) OpenPayment(ctx context.Context, payment DataStructures.Payment) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "OpenPayment", "Logic")
	defer span.End()

	return c.db.OpenPayment(ctx, payment)
}

func (c *Controller) ClosePayment(ctx context.Context, paymentID primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "ClosePayment", "Logic")
	defer span.End()

	return c.db.ClosePayment(ctx, paymentID)
}
