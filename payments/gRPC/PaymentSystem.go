package gRPC

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Payment_gRPC"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (s *gRPCServer) Ping(_ context.Context, _ *Common_gRPC.Empty) (*Common_gRPC.Status, error) {
	return &Common_gRPC.Status{
		Online: true,
		Title:  "Payments",
	}, nil
}

func (s *gRPCServer) GetPayments(ctx context.Context, _ *Common_gRPC.Empty) (*Payment_gRPC.Payments, error) {
	payments, err := s.c.GetPayments(ctx)
	if err != nil {
		return nil, err
	}

	gRPCPayments := make([]*Payment_gRPC.Payment, len(payments))
	for i := range payments {
		gRPCPayments[i] = &Payment_gRPC.Payment{
			Id:           payments[i].Id.Hex(),
			Purpose:      payments[i].Purpose,
			Price:        payments[i].Price,
			ForOrder:     payments[i].ForOrder,
			PaymentState: int64(payments[i].PaymentState),
		}
	}

	return &Payment_gRPC.Payments{Payments: gRPCPayments}, nil
}

func (s *gRPCServer) OpenPayment(ctx context.Context, data *Payment_gRPC.Payment) (paymentG *Payment_gRPC.Payment, err error) {
	payment := DataStructures.Payment{
		Purpose:      data.Purpose,
		Price:        data.Price,
		ForOrder:     data.ForOrder,
		PaymentState: int(data.PaymentState),
	}
	id, err := s.c.OpenPayment(ctx, payment)
	if err != nil {
		return nil, err
	}
	data.Id = id.Hex()
	return data, nil
}

func (s *gRPCServer) GetPaymentById(ctx context.Context, id *Common_gRPC.StringData) (*Payment_gRPC.Payment, error) {
	paymentId, err := primitive.ObjectIDFromHex(id.Data)
	if err != nil {
		return nil, err
	}
	payment, err := s.c.GetPaymentById(ctx, paymentId)
	if err != nil {
		return nil, err
	}
	return &Payment_gRPC.Payment{
		Id:           payment.Id.Hex(),
		Purpose:      payment.Purpose,
		Price:        payment.Price,
		ForOrder:     payment.ForOrder,
		PaymentState: int64(payment.PaymentState),
	}, err
}

func (s *gRPCServer) ClosePayment(ctx context.Context, data *Common_gRPC.StringData) (*Common_gRPC.StringData, error) {
	paymentId, err := primitive.ObjectIDFromHex(data.Data)
	if err != nil {
		return nil, err
	}

	err = s.c.ClosePayment(ctx, paymentId)
	if err != nil {
		return nil, err
	}

	return &Common_gRPC.StringData{
		Data: "Deleted",
	}, nil
}
