//go:generate protoc Authentication.proto --go_out=./Generated/Authentication_gRPC --go-grpc_out=./Generated/Authentication_gRPC --proto_path=DataStructures/Proto --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative
//go:generate protoc Payment.proto --go_out=./Generated/Payment_gRPC --go-grpc_out=./Generated/Payment_gRPC --proto_path=DataStructures/Proto --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative
//go:generate protoc Service.proto --go_out=./Generated/Service_gRPC --go-grpc_out=./Generated/Service_gRPC --proto_path=DataStructures/Proto --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative
//go:generate protoc Status.proto --go_out=./Generated/Status_gRPC --go-grpc_out=./Generated/Status_gRPC --proto_path=DataStructures/Proto --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative
//go:generate protoc Supply.proto --go_out=./Generated/Supply_gRPC --go-grpc_out=./Generated/Supply_gRPC --proto_path=DataStructures/Proto --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative
//go:generate protoc Tickets.proto --go_out=./Generated/Ticketing_gRPC --go-grpc_out=./Generated/Ticketing_gRPC --proto_path=DataStructures/Proto --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative
//go:generate protoc Common.proto --go_out=./Generated/Common_gRPC --go-grpc_out=./Generated/Common_gRPC --proto_path=DataStructures/Proto --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative
//go:generate protoc --js_out=import_style=commonjs,binary:Generated/JS --ts_out=service=grpc-web:Generated/JS --proto_path=DataStructures/Proto Common.proto Authentication.proto
package sua_microservices
