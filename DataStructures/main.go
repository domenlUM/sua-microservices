package DataStructures

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	Id       primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Email    string             `json:"email" bson:"email"`
	Password string             `json:"password,omitempty" bson:"password"`
	IsAdmin  bool               `json:"is_admin,omitempty" bson:"is_admin"`
}
type Users []User

type UserLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Status struct {
	Status  bool   `json:"status"`
	Service string `json:"service"`
}

type Ticket struct {
	Id               primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	ShortDescription string             `json:"short_description" bson:"short_description"`
	User             User               `json:"user" bson:"user"`
	Description      string             `json:"description" bson:"description"`
	Date             string             `json:"date" bson:"date"`
	TicketState      int                `json:"ticket_state" bson:"ticket_state"`
}

type Payment struct {
	Id           primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Purpose      string             `json:"purpose" bson:"purpose"`
	Price        float64            `json:"price" bson:"price"`
	ForOrder     string             `json:"for_order" bson:"for_order"`
	PaymentState int                `json:"payment_state" bson:"payment_state"`
}

type Supplier struct {
	SupplierId  primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name        string             `json:"name" bson:"name"`
	Address     string             `json:"address" bson:"address"`
	PhoneNumber string             `json:"description" bson:"description"`
	Email       string             `json:"email" bson:"email"`
}

type Item struct {
	ItemId  primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name    string             `json:"name" bson:"name"`
	Model   string             `json:"model" bson:"model"`
	InStock bool               `json:"in_stock" bson:"in_stock"`
}

type Order struct {
	OrderId      primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	OrderedBy    string             `json:"ordered_by" bson:"ordered_by"`
	Price        float64            `json:"price" bson:"price"`
	OrderedItems []Item             `json:"ordered_items" bson:"ordered_items"`
	OrderedFrom  string             `json:"ordered_from" bson:"ordered_from"`
}

type Service struct {
	Id          primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Purpose     string             `json:"purpose" bson:"purpose"`
	Description string             `json:"description" bson:"description"`
	Price       float64            `json:"price" bson:"price"`
	Status      Status             `json:"status" bson:"status"`
	ListOfUsers Users              `json:"list_of_users" bson:"list_of_users"`
}
