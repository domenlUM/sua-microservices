package gRPC

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
)

func (s *gRPCServer) GetOrders(ctx context.Context, _ *Common_gRPC.Empty) (*Common_gRPC.Orders, error) {
	orders, err := s.c.GetOrders(ctx)
	if err != nil {
		return nil, err
	}

	ordersGrpc := make([]*Common_gRPC.Order, len(orders))
	for i, order := range orders {
		orderedItems := make([]*Common_gRPC.Item, len(order.OrderedItems))
		for j, item := range order.OrderedItems {
			orderedItems[j] = &Common_gRPC.Item{
				Name:    item.Name,
				Model:   item.Model,
				InStock: item.InStock,
				ItemId:  item.ItemId.Hex(),
			}
		}
		suppId, _ := primitive.ObjectIDFromHex(order.OrderedFrom)
		orderSupplier, err1 := s.c.GetSupplierById(ctx, suppId)
		if err1 != nil {
			err1 = nil
		}
		ordersGrpc[i] = &Common_gRPC.Order{
			OrderId:      order.OrderId.Hex(),
			OrderedBy:    order.OrderedBy,
			Price:        order.Price,
			OrderedItems: &Common_gRPC.Items{Items: orderedItems},
			OrderedFrom: &Common_gRPC.Supplier{
				SupplierId:  orderSupplier.SupplierId.Hex(),
				Name:        orderSupplier.Name,
				Address:     orderSupplier.Address,
				PhoneNumber: orderSupplier.PhoneNumber,
				Email:       orderSupplier.Email,
			},
		}
	}

	return &Common_gRPC.Orders{Orders: ordersGrpc}, nil
}

func (s *gRPCServer) GetOrderById(ctx context.Context, data *Common_gRPC.StringData) (*Common_gRPC.Order, error) {
	orderId, err := primitive.ObjectIDFromHex(data.Data)
	if err != nil {
		return nil, err
	}

	order, err := s.c.GetOrderById(ctx, orderId)
	if err != nil {
		return nil, err
	}

	orderedItems := make([]*Common_gRPC.Item, len(order.OrderedItems))
	for j, item := range order.OrderedItems {
		orderedItems[j] = &Common_gRPC.Item{
			Name:    item.Name,
			Model:   item.Model,
			InStock: item.InStock,
			ItemId:  item.ItemId.Hex(),
		}
	}

	suppId, _ := primitive.ObjectIDFromHex(order.OrderedFrom)
	orderSupplier, err1 := s.c.GetSupplierById(ctx, suppId)
	if err1 != nil {
		err1 = nil
	}

	orderGrpc := &Common_gRPC.Order{
		OrderId:      order.OrderId.Hex(),
		OrderedBy:    order.OrderedBy,
		Price:        order.Price,
		OrderedItems: &Common_gRPC.Items{Items: orderedItems},
		OrderedFrom: &Common_gRPC.Supplier{
			SupplierId:  orderSupplier.SupplierId.Hex(),
			Name:        orderSupplier.Name,
			Address:     orderSupplier.Address,
			PhoneNumber: orderSupplier.PhoneNumber,
			Email:       orderSupplier.Email,
		},
	}

	return orderGrpc, nil
}

func (s *gRPCServer) GetItems(ctx context.Context, _ *Common_gRPC.Empty) (*Common_gRPC.Items, error) {
	items, err := s.c.GetItems(ctx)
	if err != nil {
		return nil, err
	}

	gRPCItems := make([]*Common_gRPC.Item, len(items))
	for i := range items {
		gRPCItems[i] = &Common_gRPC.Item{
			ItemId:  items[i].ItemId.Hex(),
			Name:    items[i].Name,
			Model:   items[i].Model,
			InStock: items[i].InStock,
		}
	}

	return &Common_gRPC.Items{Items: gRPCItems}, nil
}

func (s *gRPCServer) GetItemById(ctx context.Context, data *Common_gRPC.StringData) (itemGrpc *Common_gRPC.Item, err error) {
	itemId, err := primitive.ObjectIDFromHex(data.Data)
	if err != nil {
		return
	}
	item, err := s.c.GetItemById(ctx, itemId)
	if err != nil {
		return
	}

	itemGrpc = &Common_gRPC.Item{
		ItemId:  item.ItemId.Hex(),
		Name:    item.Name,
		Model:   item.Model,
		InStock: item.InStock,
	}

	return
}

func (s *gRPCServer) GetSuppliers(ctx context.Context, _ *Common_gRPC.Empty) (*Common_gRPC.Suppliers, error) {
	suppliers, err := s.c.GetSuppliers(ctx)
	if err != nil {
		return nil, err
	}

	gRPCSuppliers := make([]*Common_gRPC.Supplier, len(suppliers))
	for i := range suppliers {
		gRPCSuppliers[i] = &Common_gRPC.Supplier{
			SupplierId:  suppliers[i].SupplierId.Hex(),
			Name:        suppliers[i].Name,
			Address:     suppliers[i].Address,
			PhoneNumber: suppliers[i].PhoneNumber,
			Email:       suppliers[i].Email,
		}
	}

	return &Common_gRPC.Suppliers{Suppliers: gRPCSuppliers}, nil
}

func (s *gRPCServer) GetSupplierById(ctx context.Context, data *Common_gRPC.StringData) (supplierGrpc *Common_gRPC.Supplier, err error) {
	supplierId, err := primitive.ObjectIDFromHex(data.Data)
	if err != nil {
		return
	}

	supplier, err := s.c.GetSupplierById(ctx, supplierId)
	if err != nil {
		return
	}

	supplierGrpc = &Common_gRPC.Supplier{
		SupplierId:  supplier.SupplierId.Hex(),
		Name:        supplier.Name,
		Address:     supplier.Address,
		PhoneNumber: supplier.PhoneNumber,
		Email:       supplier.Email,
	}

	return
}

func (s *gRPCServer) CreateOrder(ctx context.Context, inputOrder *Common_gRPC.Order) (orderGrpc *Common_gRPC.Order, err error) {
	items := make([]DataStructures.Item, len(inputOrder.OrderedItems.Items))
	for i := range inputOrder.OrderedItems.Items {
		itemId, err1 := primitive.ObjectIDFromHex(inputOrder.OrderedItems.Items[i].ItemId)
		if err1 != nil {
			return
		}
		items[i] = DataStructures.Item{
			ItemId:  itemId,
			Name:    inputOrder.OrderedItems.Items[i].Name,
			Model:   inputOrder.OrderedItems.Items[i].Model,
			InStock: inputOrder.OrderedItems.Items[i].InStock,
		}
	}
	order := DataStructures.Order{
		OrderedItems: items,
		Price:        inputOrder.Price,
		OrderedBy:    inputOrder.OrderedBy,
		OrderedFrom:  inputOrder.OrderedFrom.SupplierId,
	}

	order.OrderId, err = s.c.CreateOrder(ctx, order)
	if err != nil {
		return
	}

	orderGrpc = inputOrder
	orderGrpc.OrderId = order.OrderId.Hex()

	return
}

func (s *gRPCServer) CancelOrder(ctx context.Context, data *Common_gRPC.StringData) (status *Common_gRPC.StringData, err error) {
	orderId, err := primitive.ObjectIDFromHex(data.Data)
	if err != nil {
		return
	}

	err = s.c.CancelOrder(ctx, orderId)
	if err != nil {
		return
	}

	status = &Common_gRPC.StringData{Data: "Canceled order"}
	return
}
