package MongoDB

import (
	"context"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (db *MongoDB) GetOrders(ctx context.Context) (orders []DataStructures.Order, err error) {
	span, ctx := apm.StartSpan(ctx, "GetOrders", "MongoDB")
	defer span.End()

	cursor, err := db.client.Database(db.database).Collection("orders").Find(ctx, bson.M{})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	err = cursor.All(ctx, &orders)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) GetOrderById(ctx context.Context, orderID primitive.ObjectID) (order DataStructures.Order, err error) {
	span, ctx := apm.StartSpan(ctx, "GetOrderById", "MongoDB")
	defer span.End()

	err = db.client.Database(db.database).Collection("orders").FindOne(ctx, bson.M{"_id": orderID}).Decode(&order)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) CreateOrder(ctx context.Context, order DataStructures.Order) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "OpenTicket", "MongoDB")
	defer span.End()

	result, err := db.client.Database(db.database).Collection("tickets").InsertOne(ctx, order)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return result.InsertedID.(primitive.ObjectID), nil
}

func (db *MongoDB) CancelOrder(ctx context.Context, orderID primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "CancelOrder", "MongoDB")
	defer span.End()

	_, err = db.client.Database(db.database).Collection("orders").DeleteOne(ctx, bson.M{"_id": orderID})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) GetItems(ctx context.Context) (items []DataStructures.Item, err error) {
	span, ctx := apm.StartSpan(ctx, "GetItems", "MongoDB")
	defer span.End()

	cursor, err := db.client.Database(db.database).Collection("items").Find(ctx, bson.M{})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	items = make([]DataStructures.Item, 0)

	err = cursor.All(ctx, &items)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) GetItemById(ctx context.Context, itemID primitive.ObjectID) (item DataStructures.Item, err error) {
	span, ctx := apm.StartSpan(ctx, "GetItemById", "MongoDB")
	defer span.End()

	err = db.client.Database(db.database).Collection("items").FindOne(ctx, bson.M{"_id": itemID}).Decode(&item)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) GetSuppliers(ctx context.Context) (suppliers []DataStructures.Supplier, err error) {
	span, ctx := apm.StartSpan(ctx, "GetSuppliers", "MongoDB")
	defer span.End()

	cursor, err := db.client.Database(db.database).Collection("suppliers").Find(ctx, bson.M{})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	suppliers = make([]DataStructures.Supplier, 0)

	err = cursor.All(ctx, &suppliers)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) GetSupplierById(ctx context.Context, supplierID primitive.ObjectID) (supplier DataStructures.Supplier, err error) {
	span, ctx := apm.StartSpan(ctx, "GetSupplierById", "MongoDB")
	defer span.End()

	err = db.client.Database(db.database).Collection("suppliers").FindOne(ctx, bson.M{"_id": supplierID}).Decode(&supplier)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}
