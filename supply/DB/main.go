package DB

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DB interface {
	Init(ctx context.Context) (err error)
	Close(ctx context.Context) (err error)
	GetOrders(ctx context.Context) (orders []DataStructures.Order, err error)
	GetOrderById(ctx context.Context, orderID primitive.ObjectID) (order DataStructures.Order, err error)
	CreateOrder(ctx context.Context, order DataStructures.Order) (id primitive.ObjectID, err error)
	CancelOrder(ctx context.Context, orderID primitive.ObjectID) (err error)
	GetItems(ctx context.Context) (items []DataStructures.Item, err error)
	GetItemById(ctx context.Context, itemID primitive.ObjectID) (item DataStructures.Item, err error)
	GetSuppliers(ctx context.Context) (suppliers []DataStructures.Supplier, err error)
	GetSupplierById(ctx context.Context, supplierID primitive.ObjectID) (supplier DataStructures.Supplier, err error)
}
