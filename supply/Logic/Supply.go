package Logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c *Controller) GetOrders(ctx context.Context) (orders []DataStructures.Order, err error) {
	span, ctx := apm.StartSpan(ctx, "GetOrders", "Logic")
	defer span.End()

	return c.db.GetOrders(ctx)
}

func (c *Controller) GetOrderById(ctx context.Context, orderID primitive.ObjectID) (order DataStructures.Order, err error) {
	span, ctx := apm.StartSpan(ctx, "GetOrderById", "Logic")
	defer span.End()

	return c.db.GetOrderById(ctx, orderID)
}

func (c *Controller) CreateOrder(ctx context.Context, order DataStructures.Order) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "CreateOrder", "Logic")
	defer span.End()

	return c.db.CreateOrder(ctx, order)
}

func (c *Controller) CancelOrder(ctx context.Context, orderID primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "CancelOrder", "Logic")
	defer span.End()

	return c.db.CancelOrder(ctx, orderID)
}

func (c *Controller) GetItems(ctx context.Context) (items []DataStructures.Item, err error) {
	span, ctx := apm.StartSpan(ctx, "GetItems", "Logic")
	defer span.End()

	return c.db.GetItems(ctx)
}

func (c *Controller) GetItemById(ctx context.Context, itemID primitive.ObjectID) (item DataStructures.Item, err error) {
	span, ctx := apm.StartSpan(ctx, "GetItemById", "Logic")
	defer span.End()

	return c.db.GetItemById(ctx, itemID)
}

func (c *Controller) GetSuppliers(ctx context.Context) (suppliers []DataStructures.Supplier, err error) {
	span, ctx := apm.StartSpan(ctx, "GetSuppliers", "Logic")
	defer span.End()

	return c.db.GetSuppliers(ctx)
}

func (c *Controller) GetSupplierById(ctx context.Context, itemID primitive.ObjectID) (supplier DataStructures.Supplier, err error) {
	span, ctx := apm.StartSpan(ctx, "GetSupplierById", "Logic")
	defer span.End()

	return c.db.GetSupplierById(ctx, itemID)
}
