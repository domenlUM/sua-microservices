package Logic

import "gitlab.com/domenlUM/sua-microservices/supply/DB"

type Controller struct {
	db     DB.DB
	secret []byte
}

func New(db DB.DB, secret []byte) *Controller {
	return &Controller{
		db:     db,
		secret: secret,
	}
}
