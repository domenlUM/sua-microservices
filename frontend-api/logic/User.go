package logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c *Controller) GetUsers(ctx context.Context) (users []DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUsers", "Logic")
	defer span.End()

	return c.authentication.GetUsers(ctx)
}

func (c *Controller) GetUserById(ctx context.Context, userId primitive.ObjectID) (user DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUserById", "Logic")
	defer span.End()

	return c.authentication.GetUserById(ctx, userId)
}

func (c *Controller) AddUser(ctx context.Context, user DataStructures.User) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "AddUser", "Logic")
	defer span.End()

	return c.authentication.AddUser(ctx, user)
}

func (c *Controller) EditUser(ctx context.Context, user DataStructures.User) (err error) {
	span, ctx := apm.StartSpan(ctx, "EditUser", "Logic")
	defer span.End()

	return c.authentication.EditUser(ctx, user)
}

func (c *Controller) RemoveUser(ctx context.Context, userId primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "RemoveUser", "Logic")
	defer span.End()

	return c.authentication.RemoveUser(ctx, userId)
}

func (c *Controller) LoginUser(ctx context.Context, userLogin DataStructures.UserLogin) (token string, err error) {
	span, ctx := apm.StartSpan(ctx, "LoginUser", "Logic")
	defer span.End()

	return c.authentication.Login(ctx, userLogin)
}

func (c *Controller) RegisterUser(ctx context.Context, userLogin DataStructures.UserLogin) (token string, err error) {
	span, ctx := apm.StartSpan(ctx, "RegisterUser", "Logic")
	defer span.End()

	return c.authentication.Register(ctx, userLogin)
}

func (c *Controller) Ping(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "Ping", "Logic")
	defer span.End()

	return c.authentication.Ping(ctx)
}
