package logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
)

func (c *Controller) GetTickets(ctx context.Context) (tickets []DataStructures.Ticket, err error) {
	span, ctx := apm.StartSpan(ctx, "GetTickets", "Logic")
	defer span.End()

	return c.ticketing.GetTickets(ctx)
}

func (c *Controller) GetTicketById(ctx context.Context, ticketId string) (ticket DataStructures.Ticket, err error) {
	span, ctx := apm.StartSpan(ctx, "GetTicketById", "Logic")
	defer span.End()

	ticket, err = c.ticketing.GetTicketById(ctx, ticketId)
	if err != nil {
		return
	}
	user, err := c.GetUserById(ctx, ticket.User.Id)
	if err != nil {
		err = nil
	} else {
		ticket.User = user
	}

	return
}

func (c *Controller) OpenTicket(ctx context.Context, ticket DataStructures.Ticket) (tickerId string, err error) {
	span, ctx := apm.StartSpan(ctx, "OpenTicket", "Logic")
	defer span.End()

	return c.ticketing.OpenTicket(ctx, ticket)
}

func (c *Controller) EditTicket(ctx context.Context, ticket DataStructures.Ticket) (err error) {
	span, ctx := apm.StartSpan(ctx, "EditTicket", "Logic")
	defer span.End()

	return c.ticketing.EditTicket(ctx, ticket)
}

func (c *Controller) CloseTicket(ctx context.Context, ticket string) (err error) {
	span, ctx := apm.StartSpan(ctx, "CloseTicket", "Logic")
	defer span.End()

	return c.ticketing.CloseTicket(ctx, ticket)
}
