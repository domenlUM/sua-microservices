package logic

import (
	"github.com/streadway/amqp"
	"gitlab.com/domenlUM/sua-microservices/clients/AuthenticationAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/PaymentsAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/ServiceAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/StatusAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/SupplyAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/TicketingAPI"
)

type Controller struct {
	jwtSecret      []byte
	authentication *AuthenticationAPI.Client
	status         *StatusAPI.Client
	ticketing      *TicketingAPI.Client
	supply         *SupplyAPI.Client
	service        *ServiceAPI.Client
	payments       *PaymentsAPI.Client
	AmqpChannel    *amqp.Channel
	Queue          *amqp.Queue
}

func New(jwtSecret []byte, authApi *AuthenticationAPI.Client, statusAPI *StatusAPI.Client, ticketingAPI *TicketingAPI.Client, supplyAPI *SupplyAPI.Client, serviceAPI *ServiceAPI.Client, paymentsAPI *PaymentsAPI.Client, amqpChannel *amqp.Channel, queue *amqp.Queue) *Controller {
	return &Controller{
		jwtSecret:      jwtSecret,
		authentication: authApi,
		status:         statusAPI,
		ticketing:      ticketingAPI,
		supply:         supplyAPI,
		service:        serviceAPI,
		payments:       paymentsAPI,
		AmqpChannel:    amqpChannel,
		Queue:          queue,
	}
}
