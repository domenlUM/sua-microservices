package logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
)

func (c *Controller) PingAll(ctx context.Context) (zdravje []DataStructures.Status, err error) {
	return c.status.PingAll(ctx)
}
