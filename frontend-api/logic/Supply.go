package logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
)

func (c *Controller) GetOrders(ctx context.Context) (orders []DataStructures.Order, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUsers", "Logic")
	defer span.End()

	return c.supply.GetOrders(ctx)
}

func (c *Controller) GetOrderById(ctx context.Context, orderId string) (order DataStructures.Order, err error) {
	span, ctx := apm.StartSpan(ctx, "GetOrderById", "Logic")
	defer span.End()

	return c.supply.GetOrderById(ctx, orderId)
}

func (c *Controller) CreateOrder(ctx context.Context, order DataStructures.Order) (err error, err2 error) {
	span, ctx := apm.StartSpan(ctx, "CreateOrder", "Logic")
	defer span.End()

	return c.supply.CreateOrder(ctx, order), err2
}

func (c *Controller) CancelOrder(ctx context.Context, orderId string) (err error) {
	span, ctx := apm.StartSpan(ctx, "CancelOrder", "Logic")
	defer span.End()

	return c.supply.CancelOrder(ctx, orderId)
}

func (c *Controller) GetItems(ctx context.Context) (items []DataStructures.Item, err error) {
	span, ctx := apm.StartSpan(ctx, "GetItems", "Logic")
	defer span.End()

	return c.supply.GetItems(ctx)
}

func (c *Controller) GetItemById(ctx context.Context, itemId string) (item DataStructures.Item, err error) {
	span, ctx := apm.StartSpan(ctx, "GetItemById", "Logic")
	defer span.End()

	return c.supply.GetItemById(ctx, itemId)
}

func (c *Controller) GetSuppliers(ctx context.Context) (suppliers []DataStructures.Supplier, err error) {
	span, ctx := apm.StartSpan(ctx, "GetSuppliers", "Logic")
	defer span.End()

	return c.supply.GetSuppliers(ctx)
}

func (c *Controller) GetSupplierById(ctx context.Context, supplierId string) (supplier DataStructures.Supplier, err error) {
	span, ctx := apm.StartSpan(ctx, "GetSupplierById", "Logic")
	defer span.End()

	return c.supply.GetSupplierById(ctx, supplierId)
}

func (c *Controller) PingSupply(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "PingSupply", "Logic")
	defer span.End()

	return c.supply.Ping(ctx)
}
