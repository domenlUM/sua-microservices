package logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c *Controller) GetServices(ctx context.Context) (services []DataStructures.Service, err error) {
	span, ctx := apm.StartSpan(ctx, "GetServices", "Logic")
	defer span.End()

	return c.service.GetServices(ctx)
}

func (c *Controller) GetServiceById(ctx context.Context, serviceId primitive.ObjectID) (service DataStructures.Service, err error) {
	span, ctx := apm.StartSpan(ctx, "GetServiceById", "Logic")
	defer span.End()

	return c.service.GetServiceById(ctx, serviceId.Hex())
}

func (c *Controller) ChangeServicePrice(ctx context.Context, service string, newPrice float64) (err error) {
	span, ctx := apm.StartSpan(ctx, "ChangeServicePrice", "Logic")
	defer span.End()

	return c.service.ChangeServicePrice(ctx, service, newPrice)
}

func (c *Controller) PingService(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "PingService", "Logic")
	defer span.End()

	return c.service.Ping(ctx)
}
