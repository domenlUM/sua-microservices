package logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c *Controller) GetPayments(ctx context.Context) (payment []DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUsers", "Logic")
	defer span.End()

	return c.payments.GetPayments(ctx)
}

func (c *Controller) GetPaymentById(ctx context.Context, paymentId primitive.ObjectID) (payment DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "GetPaymentById", "Logic")
	defer span.End()

	return c.payments.GetPaymentById(ctx, paymentId.Hex())
}

func (c *Controller) ClosePayment(ctx context.Context, payment DataStructures.Payment) (err error) {
	span, ctx := apm.StartSpan(ctx, "ClosePayment", "Logic")
	payment.PaymentState = 2
	defer span.End()

	return c.payments.ClosePayment(ctx, payment.Id.Hex())
}

func (c *Controller) OpenPayment(ctx context.Context, payment DataStructures.Payment) (newPayment DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "OpenPayment", "Logic")
	defer span.End()

	payment.PaymentState = 0

	return c.payments.OpenPayment(ctx, payment)
}

func (c *Controller) PingPayments(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "PingPayments", "Logic")
	defer span.End()

	return c.payments.Ping(ctx)
}
