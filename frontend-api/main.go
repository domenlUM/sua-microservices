package main

import (
	"context"
	"fmt"
	"os"

	"github.com/getsentry/sentry-go"
	"github.com/streadway/amqp"
	"gitlab.com/domenlUM/sua-microservices/clients/AuthenticationAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/PaymentsAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/ServiceAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/StatusAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/SupplyAPI"
	"gitlab.com/domenlUM/sua-microservices/clients/TicketingAPI"
	"gitlab.com/domenlUM/sua-microservices/frontend-api/logic"
	"gitlab.com/domenlUM/sua-microservices/frontend-api/rest"
)

// @title SUA Microservices ISP
// @version 0.0.1.alpha
// @description SUA Microservices ISP

// @contact.name API Support

// @host sua.leš.si
// @BasePath /api/v1/
// @query.collection.format multi

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

func main() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://ae6407b6e7bd4ee1b0a44d0ff106f6fb@o1035781.ingest.sentry.io/6086072",
	})
	if err != nil {
		fmt.Printf("sentry.Init: %s\n", err)
	}

	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		sentry.CaptureException(err)
	}

	ch, err := conn.Channel()
	if err != nil {
		sentry.CaptureException(err)
	}

	q, err := ch.QueueDeclare(
		"logging", // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		sentry.CaptureException(err)
	}

	ctx := context.Background()

	authAPI := AuthenticationAPI.New()
	err = authAPI.Init(ctx, getEnv("AUTHENTICATION_API_GRPC_ADDRESS", "authentication:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	statusAPI := StatusAPI.New()
	err = statusAPI.Init(ctx, getEnv("STATUS_API_GRPC_ADDRESS", "status:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	paymentsAPI := PaymentsAPI.New()
	err = paymentsAPI.Init(ctx, getEnv("PAYMENTS_API_GRPC_ADDRESS", "payments:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	serviceAPI := ServiceAPI.New()
	err = serviceAPI.Init(ctx, getEnv("SERVICE_API_GRPC_ADDRESS", "service:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	supplyAPI := SupplyAPI.New()
	err = supplyAPI.Init(ctx, getEnv("SUPPLY_API_GRPC_ADDRESS", "supply:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	ticketingAPI := TicketingAPI.New()
	err = ticketingAPI.Init(ctx, getEnv("TICKETING_API_GRPC_ADDRESS", "ticketing:8001"))
	if err != nil {
		sentry.CaptureException(err)
	}

	logicController := logic.New([]byte(getEnv("JWT_SECRET", "")), authAPI, statusAPI, ticketingAPI, supplyAPI, serviceAPI, paymentsAPI, ch, &q)
	if err != nil {
		sentry.CaptureException(err)
		fmt.Println(err.Error())
		return
	}

	httpAPI := rest.New(logicController, []byte(getEnv("JWT_SECRET", "")))

	signalChan := make(chan os.Signal, 0)

	httpAPI.Start(ctx)
	defer httpAPI.Stop(ctx)

	<-signalChan
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
