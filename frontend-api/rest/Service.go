package rest

import (
	"github.com/getsentry/sentry-go"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// GetServices godoc
// @Summary Get all Services
// @Description Get all Services
// @ID get-all-services
// @Produce  json
// @Tags service
// @Success 200 {object} []DataStructures.Service "Service"
// @Router /service/ [get].
func (a *Controller) GetServices(c *gin.Context) {
	services, err := a.logic.GetServices(c.Request.Context())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, services)
}

// GetServiceById godoc
// @Summary Get details for Service
// @ID get-service-by-id
// @Produce json
// @Tags service
// @Param service_id path string true "ServiceId"
// @Success 200 {object} DataStructures.Service "Service"
// @Router /service/{service_id} [get].
func (a *Controller) GetServiceById(c *gin.Context) {
	serviceId, err := primitive.ObjectIDFromHex(c.Param("service_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	service, err := a.logic.GetServiceById(c.Request.Context(), serviceId)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, service)
}

// ChangeServicePrice godoc
// @Summary Change Service Price
// @Description Changes Price of Service
// @ID change-service-price
// @Produce  json
// @Tags service
// @Param service_new_price path string true "New price"
// @Param service_id path string true "ServiceId"
// @Success 200 {String} Status "Status"
// @Router /service/{service_id}/{service_new_price} [patch].
func (a *Controller) ChangeServicePrice(c *gin.Context) {
	newPrice, err := strconv.ParseFloat(c.Param("service_new_price"), 64)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	serviceId, err := primitive.ObjectIDFromHex(c.Param("service_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	err = a.logic.ChangeServicePrice(c.Request.Context(), serviceId.Hex(), newPrice)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}
