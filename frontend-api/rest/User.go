package rest

import (
	"net/http"

	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// getUsers godoc
// @Summary Get all Users
// @Description Get all Users
// @ID get-all-users
// @Produce  json
// @Tags auth
// @Success 200 {object} []DataStructures.User "Users"
// @Router /user/ [get].
func (a *Controller) getUsers(c *gin.Context) {
	users, err := a.logic.GetUsers(c.Request.Context())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, users)
}

// getUserById godoc
// @Summary Get details for User
// @ID get-user-by-id
// @Produce  json
// @Tags auth
// @Param user_id path string true "UserId"
// @Success 200 {object} DataStructures.User "User"
// @Router /user/{user_id} [get].
func (a *Controller) getUserById(c *gin.Context) {
	userId, err := primitive.ObjectIDFromHex(c.Param("user_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user, err := a.logic.GetUserById(c.Request.Context(), userId)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, user)
}

// addUser godoc
// @Summary Add User
// @Description Adds User
// @ID add-user
// @Produce  json
// @Tags auth
// @Param Users body DataStructures.User true "User"
// @Success 200 {String} Status "Status"
// @Router /user/ [post].
func (a *Controller) addUser(c *gin.Context) {
	var user DataStructures.User
	err := c.BindJSON(&user)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	id, err := a.logic.AddUser(c.Request.Context(), user)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": id})
}

// editUser godoc
// @Summary Edit User
// @Description Edits User
// @ID edit-user
// @Produce  json
// @Tags auth
// @Param Users body DataStructures.User true "User"
// @Success 200 {String} Status "Status"
// @Router /user/{user_id} [patch].
func (a *Controller) editUser(c *gin.Context) {
	var user DataStructures.User
	err := c.BindJSON(&user)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = a.logic.EditUser(c.Request.Context(), user)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

// removeUser godoc
// @Summary Remove User
// @Description Remove User
// @ID remove-user
// @Tags auth
// @Param user_id path string true "UserId"
// @Success 200 {string} Status "Status"
// @Router /user/{user_id} [delete].
func (a *Controller) removeUser(c *gin.Context) {
	userId, err := primitive.ObjectIDFromHex(c.Param("user_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = a.logic.RemoveUser(c.Request.Context(), userId)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

// LoginUser godoc
// @Summary Login user account
// @Description Login user and retrieve JWT for session
// @ID login-user
// @Accept  json
// @Produce  json
// @Tags auth
// @Param UserLogin body DataStructures.UserLogin true "Login details"
// @Success 200 {string} Token "JWT"
// @Router /login [post].
func (a *Controller) LoginUser(c *gin.Context) {
	var userLogin DataStructures.UserLogin
	err := c.BindJSON(&userLogin)
	if err != nil {
		sentry.CaptureException(err)
		err1 := c.AbortWithError(http.StatusInternalServerError, err)
		if err1 != nil {
			sentry.CaptureException(err1)
		}
		return
	}

	token, err := a.logic.LoginUser(c.Request.Context(), userLogin)
	if err != nil {
		sentry.CaptureException(err)
		err1 := c.AbortWithError(http.StatusInternalServerError, err)
		if err1 != nil {
			sentry.CaptureException(err1)
		}
		return
	}

	c.SetCookie("JWT", token, 3153600000, "/", c.Request.URL.Host, false, true)

	c.JSON(http.StatusOK, gin.H{"token": token})
}

// RegisterUser godoc
// @Summary Register user account
// @Description Register user and retrieve JWT for session
// @ID register-user
// @Accept  json
// @Produce  json
// @Tags auth
// @Param UserLogin body DataStructures.UserLogin true "Register details"
// @Success 200 {string} Token "JWT"
// @Router /register [post].
func (a *Controller) RegisterUser(c *gin.Context) {
	var userLogin DataStructures.UserLogin
	err := c.BindJSON(&userLogin)
	if err != nil {
		sentry.CaptureException(err)
		err1 := c.AbortWithError(http.StatusInternalServerError, err)
		if err1 != nil {
			sentry.CaptureException(err1)
		}
		return
	}

	token, err := a.logic.RegisterUser(c.Request.Context(), userLogin)
	if err != nil {
		sentry.CaptureException(err)
		err1 := c.AbortWithError(http.StatusInternalServerError, err)
		if err1 != nil {
			sentry.CaptureException(err1)
		}
		return
	}

	c.SetCookie("JWT", token, 3153600000, "/", c.Request.URL.Host, false, true)

	c.JSON(http.StatusOK, gin.H{"token": token})
}
