package rest

import (
	"github.com/getsentry/sentry-go"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// GetPayments godoc
// @Summary Get all Payments
// @Description Get all Payments
// @ID get-all-payments
// @Produce json
// @Tags payments
// @Success 200 {object} []DataStructures.Payment "Payments"
// @Router /payment/ [get].
func (a *Controller) GetPayments(c *gin.Context) {
	users, err := a.logic.GetPayments(c.Request.Context())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, users)
}

// GetPaymentById godoc
// @Summary Get details for Payment
// @ID get-payment-by-id
// @Produce  json
// @Tags payments
// @Param payment_id path string true "PaymentId"
// @Success 200 {object} DataStructures.Payment "Payment"
// @Router /payment/{payment_id} [get].
func (a *Controller) GetPaymentById(c *gin.Context) {
	paymentId, err := primitive.ObjectIDFromHex(c.Param("payment_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	payment, err := a.logic.GetPaymentById(c.Request.Context(), paymentId)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, payment)
}

// OpenPayment godoc
// @Summary Open Payment
// @Description Open Payment
// @ID open-payment
// @Produce  json
// @Tags payments
// @Param Payments body DataStructures.Payment true "Payment"
// @Success 200 {String} Status "Status"
// @Router /payment/ [post].
func (a *Controller) OpenPayment(c *gin.Context) {
	var payment DataStructures.Payment
	err := c.BindJSON(&payment)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	newPayment, err := a.logic.OpenPayment(c.Request.Context(), payment)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, newPayment)
}

// ClosePayment godoc
// @Summary Close Payment
// @Description Closes Payment
// @ID close-payment
// @Produce  json
// @Tags payments
// @Param Payments body DataStructures.Payment true "Payment"
// @Success 200 {String} Status "Status"
// @Router /payment/ [patch].
func (a *Controller) ClosePayment(c *gin.Context) {
	var payment DataStructures.Payment
	err := c.BindJSON(&payment)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = a.logic.ClosePayment(c.Request.Context(), payment)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}
