package rest

import (
	"github.com/getsentry/sentry-go"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// GetOrders godoc
// @Summary Get all Orders
// @Description Get all Orders
// @ID get-all-orders
// @Produce  json
// @Tags supply
// @Success 200 {object} []DataStructures.Order "Orders"
// @Router /supply/orders [get].
func (a *Controller) GetOrders(c *gin.Context) {
	orders, err := a.logic.GetOrders(c.Request.Context())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, orders)
}

// GetOrderById godoc
// @Summary Get details for Order
// @ID get-order-by-id
// @Produce  json
// @Tags supply
// @Param order_id path string true "OrderId"
// @Success 200 {object} DataStructures.Order "Order"
// @Router /supply/orders/{order_id} [get].
func (a *Controller) GetOrderById(c *gin.Context) {
	orderId, err := primitive.ObjectIDFromHex(c.Param("order_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	order, err := a.logic.GetOrderById(c.Request.Context(), orderId.Hex())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, order)
}

// CreateOrder godoc
// @Summary Create Order
// @Description Creates Order
// @ID create-order
// @Produce  json
// @Tags supply
// @Param Orders body DataStructures.Order true "Order"
// @Success 200 {String} Status "Status"
// @Router /supply/orders [put].
func (a *Controller) CreateOrder(c *gin.Context) {
	var order DataStructures.Order
	err := c.BindJSON(&order)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	id, err := a.logic.CreateOrder(c.Request.Context(), order)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": id})
}

// CancelOrder godoc
// @Summary Cancel Order
// @Description Cancels Order
// @ID cancel-user
// @Produce  json
// @Tags supply
// @Param order_id path string true "OrderId"
// @Success 200 {String} Status "Status"
// @Router /supply/orders/{order_id}/cancel [post].
func (a *Controller) CancelOrder(c *gin.Context) {
	err := a.logic.CancelOrder(c.Request.Context(), c.Param("order_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

// GetItems godoc
// @Summary Get all items
// @Description Get all Items
// @ID get-all-items
// @Produce  json
// @Tags supply
// @Success 200 {object} []DataStructures.Item "Items"
// @Router /supply/items [get].
func (a *Controller) GetItems(c *gin.Context) {
	users, err := a.logic.GetItems(c.Request.Context())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, users)
}

// GetItemById godoc
// @Summary Get details for Item
// @ID get-item-by-id
// @Produce  json
// @Tags supply
// @Param item_id path string true "ItemId"
// @Success 200 {object} DataStructures.User "Item"
// @Router /supply/items/{item_id} [get].
func (a *Controller) GetItemById(c *gin.Context) {
	itemId, err := primitive.ObjectIDFromHex(c.Param("item_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	item, err := a.logic.GetItemById(c.Request.Context(), itemId.Hex())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, item)
}

// GetSuppliers godoc
// @Summary Get all Suppliers
// @Description Get all Suppliers
// @ID get-all-suppliers
// @Produce  json
// @Tags supply
// @Success 200 {object} []DataStructures.Supplier "Suppliers"
// @Router /supply/suppliers [get].
func (a *Controller) GetSuppliers(c *gin.Context) {
	users, err := a.logic.GetSuppliers(c.Request.Context())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, users)
}

// GetSupplierById godoc
// @Summary Get details for Supply
// @ID get-supply-by-id
// @Produce  json
// @Tags supply
// @Param supply_id path string true "SupplyId"
// @Success 200 {object} DataStructures.Supplier "Supplier"
// @Router /supply/suppliers/{supply_id} [get].
func (a *Controller) GetSupplierById(c *gin.Context) {
	supplyId, err := primitive.ObjectIDFromHex(c.Param("supply_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	supply, err := a.logic.GetSupplierById(c.Request.Context(), supplyId.Hex())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, supply)
}
