package rest

import (
	"github.com/getsentry/sentry-go"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// GetTickets godoc
// @Summary Get all Tickets
// @Description Get all Tickets
// @ID get-all-tickets
// @Produce  json
// @Tags ticketing
// @Success 200 {object} []DataStructures.Ticket "Tickets"
// @Router /ticketing/ [get].
func (a *Controller) GetTickets(c *gin.Context) {
	tickets, err := a.logic.GetTickets(c.Request.Context())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, tickets)
}

// GetTicketById godoc
// @Summary Get details for Ticket
// @ID get-ticket-by-id
// @Produce  json
// @Tags ticketing
// @Param ticket_id path string true "TicketId"
// @Success 200 {object} DataStructures.Ticket "Ticket"
// @Router /ticketing/{ticket_id} [get].
func (a *Controller) GetTicketById(c *gin.Context) {
	ticketId, err := primitive.ObjectIDFromHex(c.Param("ticket_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ticket, err := a.logic.GetTicketById(c.Request.Context(), ticketId.Hex())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, ticket)
}

// OpenTicket godoc
// @Summary OpenTicket
// @Description Adds a ticket - opens it
// @ID open-ticket
// @Produce  json
// @Tags ticketing
// @Param Ticket body DataStructures.Ticket true "Ticket"
// @Success 200 {String} Status "Status"
// @Router /ticketing/ [post].
func (a *Controller) OpenTicket(c *gin.Context) {
	var ticket DataStructures.Ticket
	err := c.BindJSON(&ticket)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	id, err := a.logic.OpenTicket(c.Request.Context(), ticket)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": id})
}

// EditTicket godoc
// @Summary Edit Ticket
// @Description Edits Ticket
// @ID edit-ticket
// @Produce  json
// @Tags ticketing
// @Param Ticket body DataStructures.Ticket true "Ticket"
// @Success 200 {String} Status "Status"
// @Router /ticketing/ [patch].
func (a *Controller) EditTicket(c *gin.Context) {
	var ticket DataStructures.Ticket
	err := c.BindJSON(&ticket)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = a.logic.EditTicket(c.Request.Context(), ticket)
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

// CloseTicket godoc
// @Summary Close Ticket
// @Description Closes Ticket - set status closed
// @ID close-ticket
// @Produce  json
// @Tags ticketing
// @Param ticket_id path string true "TicketId"
// @Success 200 {String} Status "Status"
// @Router /ticketing/{ticket_id}/close [patch].
func (a *Controller) CloseTicket(c *gin.Context) {
	err := a.logic.CloseTicket(c.Request.Context(), c.Param("ticket_id"))
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}
