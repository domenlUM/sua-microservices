package rest

import (
	"context"
	"errors"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/streadway/amqp"
	"gitlab.com/domenlUM/sua-microservices/frontend-api/logic"
	"go.elastic.co/apm/module/apmgin"
)

type Controller struct {
	logic  *logic.Controller
	done   chan bool
	secret []byte
}

func New(logic *logic.Controller, secret []byte) *Controller {
	return &Controller{
		logic:  logic,
		secret: secret,
	}
}

func (a *Controller) GetLogicController() *logic.Controller {
	return a.logic
}

func (a *Controller) Start(_ context.Context) {
	engine := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowMethods = []string{"PUT", "POST", "GET", "OPTIONS", "DELETE"}
	engine.Use(a.addAndLogCorrelationId())
	engine.Use(cors.New(config))
	engine.Use(apmgin.Middleware(engine))
	engine.Use(sentrygin.New(sentrygin.Options{}))
	engine.Use(a.checkAuth())
	a.registerRoutes(engine)

	a.done = make(chan bool)

	srv := &http.Server{
		Addr:         ":8000",
		Handler:      engine,
		ReadTimeout:  20 * time.Second,
		WriteTimeout: 30 * time.Second,
	}
	go func() {
		<-a.done

		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		defer cancel()

		srv.SetKeepAlivesEnabled(false)
		if err := srv.Shutdown(ctx); err != nil {
			sentry.CaptureException(err)
		}
		os.Exit(1)
	}()
	go func() {
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			sentry.CaptureException(err)
		}
	}()
}

func (a *Controller) Stop(_ context.Context) {
	a.done <- true
}

func (a *Controller) addAndLogCorrelationId() gin.HandlerFunc {
	return func(c *gin.Context) {
		generatedUUID := uuid.New().String()
		c.Set("correlationId", generatedUUID)

		c.Request = c.Request.Clone(context.WithValue(c.Request.Context(), "correlationId", generatedUUID))

		err := a.logic.AmqpChannel.Publish(
			"",                 // exchange
			a.logic.Queue.Name, // routing key
			false,              // mandatory
			false,              // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(time.Now().String() + " INFO FRONTEND-API " + c.Request.RequestURI + " CORRELATION:" + generatedUUID),
			})
		if err != nil {
			sentry.CaptureException(err)
		}

		c.Next()
	}
}

func (a *Controller) checkAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Izjema za login in swagger docs
		if strings.HasPrefix(c.Request.RequestURI, "/api/v1/register") || strings.HasPrefix(c.Request.RequestURI, "/api/v1/login") || strings.HasPrefix(c.Request.RequestURI, "/api/v1/swagger/") {
			return
		}

		jwtString := ""
		var err error = nil

		tokenHeader := c.GetHeader("Authorization")
		if len(tokenHeader) > 0 {

			headerArr := strings.Split(tokenHeader, " ")
			if len(headerArr) != 2 {
				c.AbortWithStatus(http.StatusUnauthorized)
				return
			}
			jwtString = headerArr[1]

		} else {
			jwtString, err = c.Cookie("JWT")
			if err != nil {
				c.AbortWithStatus(http.StatusUnauthorized)
			}
		}
		if jwtString == "" {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		token, err := jwt.Parse(jwtString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				sentry.CaptureException(errors.New("error checking JWT token"))
				c.AbortWithStatus(http.StatusUnauthorized)
				return nil, errors.New("there was an error")
			}
			return a.secret, nil
		})
		if err != nil {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			c.Set("userId", claims["user_id"])
		} else {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		c.Next()
	}
}
