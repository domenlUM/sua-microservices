package rest

import (
	"github.com/getsentry/sentry-go"
	"net/http"

	"github.com/gin-gonic/gin"
)

// PingAll godoc
// @Summary Get status of all services
// @Description Get status of all services
// @ID ping-status-service
// @Produce  json
// @Tags status
// @Success 200 {object} []DataStructures.Status "OK"
// @Router /status/ [get]
func (a *Controller) PingAll(c *gin.Context) {
	status, err := a.logic.PingAll(c.Request.Context())
	if err != nil {
		sentry.CaptureException(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, status)
}
