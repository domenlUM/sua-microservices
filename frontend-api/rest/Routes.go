package rest

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/domenlUM/sua-microservices/frontend-api/docs"
)

func (a *Controller) registerRoutes(c *gin.Engine) {
	api := c.Group("/api/v1/")

	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	api.POST("/login", a.LoginUser)
	api.POST("/register", a.RegisterUser)

	a.registerUserRoutes(api.Group("/user"))

	a.registerStatusRoutes(api.Group("/status"))

	a.registerTicketingRoutes(api.Group("/ticketing"))

	a.registerSupplyRoutes(api.Group("/supply"))

	a.registerServiceRoutes(api.Group("/service"))
	a.registerPaymentRoutes(api.Group("/payment"))
}

func (a *Controller) registerUserRoutes(user *gin.RouterGroup) {
	user.GET("/", a.getUsers)
	user.POST("/", a.addUser)
	user.GET("/:user_id", a.getUserById)
	user.PATCH("/", a.editUser)
	user.DELETE("/:user_id", a.removeUser)
}

func (a *Controller) registerStatusRoutes(status *gin.RouterGroup) {
	status.GET("/", a.PingAll)
}

func (a *Controller) registerTicketingRoutes(ticketing *gin.RouterGroup) {
	ticketing.POST("/", a.OpenTicket)
	ticketing.GET("/", a.GetTickets)
	ticketing.GET("/:ticket_id", a.GetTicketById)
	ticketing.PATCH("/", a.EditTicket)
	ticketing.PATCH("/:ticket_id/close", a.CloseTicket)
}

func (a *Controller) registerSupplyRoutes(supply *gin.RouterGroup) {
	supply.GET("/orders", a.GetOrders)
	supply.GET("/orders/:order_id", a.GetOrderById)
	supply.POST("/orders", a.CreateOrder)
	supply.POST("/orders/:order_id/cancel", a.CancelOrder)
	supply.GET("/suppliers", a.GetSuppliers)
	supply.GET("/suppliers/:supply_id", a.GetSupplierById)
	supply.GET("/items", a.GetItems)
	supply.GET("/items/:item_id", a.GetItemById)
}

func (a *Controller) registerServiceRoutes(service *gin.RouterGroup) {
	service.GET("/", a.GetServices)
	service.GET("/:service_id", a.GetServiceById)
	service.PATCH("/:service_id/:service_new_price", a.ChangeServicePrice)
}

func (a *Controller) registerPaymentRoutes(payment *gin.RouterGroup) {
	payment.GET("/", a.GetPayments)
	payment.GET("/:payment_id", a.GetPaymentById)
	payment.PATCH("/", a.ClosePayment)
	payment.POST("/", a.OpenPayment)
}
