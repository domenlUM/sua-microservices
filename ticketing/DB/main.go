package DB

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DB interface {
	Init(ctx context.Context) (err error)
	Close(ctx context.Context) (err error)
	GetTicketById(ctx context.Context, ticketID primitive.ObjectID) (ticket DataStructures.Ticket, err error)
	GetTickets(ctx context.Context) (ticket []DataStructures.Ticket, err error)
	OpenTicket(ctx context.Context, ticket DataStructures.Ticket) (id primitive.ObjectID, err error)
	EditTicket(ctx context.Context, ticket DataStructures.Ticket) (err error)
	DeleteTicket(ctx context.Context, ticketID primitive.ObjectID) (err error)
	CloseTicket(ctx context.Context, ticketID primitive.ObjectID) (err error)
}
