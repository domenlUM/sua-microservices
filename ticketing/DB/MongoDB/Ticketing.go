package MongoDB

import (
	"context"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (db *MongoDB) GetTickets(ctx context.Context) (tickets []DataStructures.Ticket, err error) {
	span, ctx := apm.StartSpan(ctx, "GetTickets", "MongoDB")
	defer span.End()

	opts := options.Find()

	cursor, err := db.client.Database(db.database).Collection("tickets").Find(ctx, bson.M{}, opts)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	tickets = make([]DataStructures.Ticket, 0)

	err = cursor.All(ctx, &tickets)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) GetTicketById(ctx context.Context, ticketID primitive.ObjectID) (ticket DataStructures.Ticket, err error) {
	span, ctx := apm.StartSpan(ctx, "GetTicketById", "MongoDB")
	defer span.End()

	err = db.client.Database(db.database).Collection("tickets").FindOne(ctx, bson.M{"_id": ticketID}).Decode(&ticket)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) OpenTicket(ctx context.Context, ticket DataStructures.Ticket) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "OpenTicket", "MongoDB")
	defer span.End()

	result, err := db.client.Database(db.database).Collection("tickets").InsertOne(ctx, ticket)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return result.InsertedID.(primitive.ObjectID), nil
}

func (db *MongoDB) EditTicket(ctx context.Context, ticket DataStructures.Ticket) (err error) {
	span, ctx := apm.StartSpan(ctx, "EditTicket", "MongoDB")
	defer span.End()

	_, err = db.client.Database(db.database).Collection("tickets").UpdateOne(ctx, bson.M{"_id": ticket.Id}, bson.M{
		"$set": ticket,
	})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) DeleteTicket(ctx context.Context, ticketID primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "DeleteTicket", "MongoDB")
	defer span.End()

	_, err = db.client.Database(db.database).Collection("tickets").DeleteOne(ctx, bson.M{"_id": ticketID})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) CloseTicket(ctx context.Context, ticketID primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "CloseTicket", "MongoDB")
	defer span.End()

	_, err = db.client.Database(db.database).Collection("tickets").UpdateOne(ctx, bson.M{"_id": ticketID}, bson.M{
		"$set": bson.M{"ticket_state": 1},
	})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}
