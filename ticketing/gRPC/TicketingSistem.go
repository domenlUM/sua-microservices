package gRPC

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"

	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Ticketing_gRPC"
)

func (s *gRPCServer) Ping(_ context.Context, _ *Common_gRPC.Empty) (*Common_gRPC.Status, error) {
	return &Common_gRPC.Status{
		Online: true,
		Title:  "Ticketing",
	}, nil
}

func (s *gRPCServer) GetTickets(ctx context.Context, _ *Common_gRPC.Empty) (*Ticketing_gRPC.Tickets, error) {
	tickets, err := s.c.GetTickets(ctx)
	if err != nil {
		return nil, err
	}

	gRPCTickets := make([]*Ticketing_gRPC.Ticket, len(tickets))
	for i := range tickets {
		gRPCTickets[i] = &Ticketing_gRPC.Ticket{
			Id:               tickets[i].Id.Hex(),
			ShortDescription: tickets[i].ShortDescription,
			SubmittedBy:      tickets[i].User.Id.Hex(),
			Description:      tickets[i].Description,
			Date:             tickets[i].Date,
		}
	}

	return &Ticketing_gRPC.Tickets{Tickets: gRPCTickets}, nil
}

func (s *gRPCServer) OpenTicket(ctx context.Context, data *Ticketing_gRPC.Ticket) (output *Ticketing_gRPC.Ticket, err error) {
	ticket := DataStructures.Ticket{
		ShortDescription: data.ShortDescription,
		Description:      data.Description,
		Date:             data.Date,
	}

	id, err := s.c.OpenTicket(ctx, ticket)

	output = &Ticketing_gRPC.Ticket{
		Id:               id.Hex(),
		ShortDescription: data.ShortDescription,
		SubmittedBy:      data.SubmittedBy,
		Description:      data.Description,
		Date:             data.Date,
	}

	return
}

func (s *gRPCServer) GetTicketById(ctx context.Context, data *Common_gRPC.StringData) (output *Ticketing_gRPC.Ticket, err error) {
	ticketId, err := primitive.ObjectIDFromHex(data.Data)
	if err != nil {
		return nil, err
	}

	ticket, err := s.c.GetTicketById(ctx, ticketId)
	if err != nil {
		return nil, err
	}

	output = &Ticketing_gRPC.Ticket{
		Id:               ticket.Id.Hex(),
		ShortDescription: ticket.ShortDescription,
		SubmittedBy:      ticket.User.Id.Hex(),
		Description:      ticket.Description,
		Date:             ticket.Date,
	}

	return
}

func (s *gRPCServer) EditTicket(ctx context.Context, data *Ticketing_gRPC.Ticket) (output *Ticketing_gRPC.Ticket, err error) {
	ticketId, err := primitive.ObjectIDFromHex(data.Id)

	ticket := DataStructures.Ticket{
		Id:               ticketId,
		ShortDescription: data.ShortDescription,
		Description:      data.Description,
		Date:             data.Date,
	}

	err = s.c.EditTicket(ctx, ticket)

	return
}

func (s *gRPCServer) DeleteTicket(ctx context.Context, data *Common_gRPC.StringData) (output *Common_gRPC.StringData, err error) {
	ticketId, err := primitive.ObjectIDFromHex(data.Data)

	err = s.c.DeleteTicket(ctx, ticketId)
	if err != nil {
		return nil, err
	} else {
		return &Common_gRPC.StringData{Data: "Ticket deleted"}, nil
	}
}

func (s *gRPCServer) CloseTicket(ctx context.Context, data *Common_gRPC.StringData) (output *Ticketing_gRPC.Ticket, err error) {
	ticketId, err := primitive.ObjectIDFromHex(data.Data)

	err = s.c.CloseTicket(ctx, ticketId)
	if err != nil {
		return nil, err
	}
	return s.GetTicketById(ctx, &Common_gRPC.StringData{Data: data.Data})
}
