package Logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c *Controller) GetTickets(ctx context.Context) (tickets []DataStructures.Ticket, err error) {
	span, ctx := apm.StartSpan(ctx, "GetTickets", "Logic")
	defer span.End()

	return c.db.GetTickets(ctx)
}

func (c *Controller) GetTicketById(ctx context.Context, ticketID primitive.ObjectID) (ticket DataStructures.Ticket, err error) {
	span, ctx := apm.StartSpan(ctx, "GetTicketById", "Logic")
	defer span.End()

	return c.db.GetTicketById(ctx, ticketID)
}

func (c *Controller) OpenTicket(ctx context.Context, ticket DataStructures.Ticket) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "OpenTicket", "Logic")
	defer span.End()

	return c.db.OpenTicket(ctx, ticket)
}

func (c *Controller) EditTicket(ctx context.Context, ticket DataStructures.Ticket) (err error) {
	span, ctx := apm.StartSpan(ctx, "EditTicket", "Logic")
	defer span.End()

	return c.db.EditTicket(ctx, ticket)
}

func (c *Controller) DeleteTicket(ctx context.Context, ticketID primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "DeleteTicket", "Logic")
	defer span.End()

	return c.db.DeleteTicket(ctx, ticketID)
}

func (c *Controller) CloseTicket(ctx context.Context, ticketID primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "CloseTicket", "Logic")
	defer span.End()

	return c.db.CloseTicket(ctx, ticketID)
}
