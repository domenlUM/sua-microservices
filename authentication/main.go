package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/streadway/amqp"
	"gitlab.com/domenlUM/sua-microservices/authentication/DB/MongoDB"
	"gitlab.com/domenlUM/sua-microservices/authentication/Logic"
	"gitlab.com/domenlUM/sua-microservices/authentication/gRPC"
)

func main() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://69440f8d9eed42f3a3e2d5fcd771fa47@o1035781.ingest.sentry.io/6083522",
	})
	if err != nil {
		fmt.Printf("sentry.Init: %s\n", err)
	}

	defer func() { sentry.Flush(5 * time.Second) }()

	ctx := context.Background()

	mongoDB := MongoDB.New(
		getEnv("MONGO_USER", "root"),
		getEnv("MONGO_PASS", "root"),
		getEnv("MONGO_IP", "mongo-auth"),
		getEnv("MONGO_PORT", "27017"),
		getEnv("MONGO_DB", "authentication"),
		getEnv("MONGO_AUTH_DB", "admin"),
		getEnv("MONGO_AUTH_MECHANISM", "SCRAM-SHA-256"))
	err = mongoDB.Init(ctx)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	conn, err := amqp.Dial(getEnv("RABBIT_URL", "amqp://guest:guest@rabbitmq:5672/"))
	if err != nil {
		sentry.CaptureException(err)
	}

	ch, err := conn.Channel()
	if err != nil {
		sentry.CaptureException(err)
	}

	q, err := ch.QueueDeclare(
		"logging", // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		sentry.CaptureException(err)
	}

	logicController := Logic.New(mongoDB, []byte(getEnv("JWT_SECRET", "")))
	if err != nil {
		sentry.CaptureException(err)
		fmt.Println(err.Error())
		return
	}

	grpcAPI := gRPC.New(logicController)

	signalChan := make(chan os.Signal, 0)
	closeGRPC := make(chan bool, 0)

	grpcAPI.Start(closeGRPC, ch, &q)

	<-signalChan

	close(closeGRPC)
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
