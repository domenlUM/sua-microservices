package MongoDB

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"

	"github.com/getsentry/sentry-go"
	"go.elastic.co/apm/module/apmmongo"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDB struct {
	client        *mongo.Client
	user          string
	pass          string
	ip            string
	port          string
	database      string
	authDB        string
	authMechanism string
}

const (
	// Timeout operations after N seconds
	connectionStringTemplate = "mongodb://%s:%s@%s:%s/?serverSelectionTimeoutMS=5000&connectTimeoutMS=10000&authSource=%s&authMechanism=%s"
)

func New(user, pass, ip, port, database, authDB, authMechanism string) *MongoDB {
	return &MongoDB{
		user:          user,
		pass:          pass,
		ip:            ip,
		port:          port,
		database:      database,
		authDB:        authDB,
		authMechanism: authMechanism,
	}
}

func (db *MongoDB) Init(ctx context.Context) (err error) {
	connectionURI := fmt.Sprintf(connectionStringTemplate, db.user, db.pass, db.ip, db.port, db.authDB, db.authMechanism)
	db.client, err = mongo.NewClient(options.Client().ApplyURI(connectionURI).SetServerAPIOptions(options.ServerAPI(options.ServerAPIVersion1)).SetMonitor(apmmongo.CommandMonitor()))
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	ctx, cancelFunc := context.WithTimeout(ctx, 3*time.Second)
	defer cancelFunc()

	err = db.client.Connect(ctx)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	_, err = db.client.Database(db.database).Collection("users").Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys: bson.M{
			"email": 1,
		},
		Options: options.Index().SetUnique(true),
	})
	if err != nil {
		sentry.CaptureException(err)
	}

	return
}

func (db *MongoDB) Close(ctx context.Context) (err error) {
	ctx, cancelFunc := context.WithTimeout(ctx, 3*time.Second)
	defer cancelFunc()

	err = db.client.Disconnect(ctx)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	return
}
