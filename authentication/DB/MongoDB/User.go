package MongoDB

import (
	"context"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (db *MongoDB) GetUsers(ctx context.Context) (users []DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUsers", "MongoDB")
	defer span.End()

	opts := options.Find()
	opts.SetSort(bson.D{{"email", 1}, {"_id", 1}})

	cursor, err := db.client.Database(db.database).Collection("users").Find(ctx, bson.M{}, opts)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	users = make([]DataStructures.User, 0)

	err = cursor.All(ctx, &users)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) GetUserById(ctx context.Context, userId primitive.ObjectID) (user DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUserById", "MongoDB")
	defer span.End()

	err = db.client.Database(db.database).Collection("users").FindOne(ctx, bson.M{"_id": userId}).Decode(&user)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) GetUserByEmail(ctx context.Context, email string) (user DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUserByEmail", "MongoDB")
	defer span.End()

	err = db.client.Database(db.database).Collection("users").FindOne(ctx, bson.M{"email": email}).Decode(&user)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) AddUser(ctx context.Context, user DataStructures.User) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "AddUser", "MongoDB")
	defer span.End()

	result, err := db.client.Database(db.database).Collection("users").InsertOne(ctx, user)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return result.InsertedID.(primitive.ObjectID), nil
}

func (db *MongoDB) EditUser(ctx context.Context, user DataStructures.User) (err error) {
	span, ctx := apm.StartSpan(ctx, "EditUser", "MongoDB")
	defer span.End()

	_, err = db.client.Database(db.database).Collection("users").UpdateOne(ctx, bson.M{"_id": user.Id}, bson.M{
		"$set": user,
	})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) RemoveUser(ctx context.Context, userId primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "RemoveUser", "MongoDB")
	defer span.End()

	_, err = db.client.Database(db.database).Collection("users").DeleteOne(ctx, bson.M{"_id": userId})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}
