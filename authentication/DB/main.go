package DB

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DB interface {
	Init(ctx context.Context) (err error)
	Close(ctx context.Context) (err error)
	GetUsers(ctx context.Context) (users []DataStructures.User, err error)
	GetUserById(ctx context.Context, userId primitive.ObjectID) (user DataStructures.User, err error)
	GetUserByEmail(ctx context.Context, email string) (user DataStructures.User, err error)
	AddUser(ctx context.Context, user DataStructures.User) (id primitive.ObjectID, err error)
	EditUser(ctx context.Context, user DataStructures.User) (err error)
	RemoveUser(ctx context.Context, userId primitive.ObjectID) (err error)
}
