package gRPC

import (
	"context"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"gitlab.com/domenlUM/sua-microservices/Generated/Authentication_gRPC"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (s *gRPCServer) Login(ctx context.Context, userLogin *Authentication_gRPC.UserLogin) (*Authentication_gRPC.Token, error) {
	span, ctx := apm.StartSpan(ctx, "Login", "gRPC")
	defer span.End()

	token, err := s.c.Login(ctx, DataStructures.UserLogin{
		Email:    userLogin.Email,
		Password: userLogin.Password,
	})
	if err != nil {
		sentry.CaptureException(err)
		return nil, err
	}

	return &Authentication_gRPC.Token{Token: token}, nil
}

func (s *gRPCServer) Register(ctx context.Context, userLogin *Authentication_gRPC.UserLogin) (*Authentication_gRPC.Token, error) {
	span, ctx := apm.StartSpan(ctx, "Register", "gRPC")
	defer span.End()

	token, err := s.c.Register(ctx, DataStructures.UserLogin{
		Email:    userLogin.Email,
		Password: userLogin.Password,
	})
	if err != nil {
		sentry.CaptureException(err)
		return nil, err
	}

	return &Authentication_gRPC.Token{Token: token}, nil
}

func (s *gRPCServer) GetUsers(ctx context.Context, _ *Authentication_gRPC.Empty) (*Authentication_gRPC.Users, error) {
	span, ctx := apm.StartSpan(ctx, "GetUsers", "gRPC")
	defer span.End()

	users, err := s.c.GetUsers(ctx)
	if err != nil {
		return nil, err
	}

	gRPCUsers := make([]*Authentication_gRPC.User, len(users))
	for i := range users {
		gRPCUsers[i] = &Authentication_gRPC.User{
			Id:      users[i].Id.Hex(),
			Email:   users[i].Email,
			IsAdmin: users[i].IsAdmin,
		}
	}

	return &Authentication_gRPC.Users{Users: gRPCUsers}, nil
}

func (s *gRPCServer) GetUserById(ctx context.Context, data *Authentication_gRPC.StringData) (*Authentication_gRPC.User, error) {
	span, ctx := apm.StartSpan(ctx, "GetUserById", "gRPC")
	defer span.End()

	userId, err := primitive.ObjectIDFromHex(data.Data)
	if err != nil {
		return nil, err
	}

	user, err := s.c.GetUserById(ctx, userId)
	if err != nil {
		return nil, err
	}

	return &Authentication_gRPC.User{
		Id:      user.Id.Hex(),
		Email:   user.Email,
		IsAdmin: user.IsAdmin,
	}, err
}

func (s *gRPCServer) EditUser(ctx context.Context, gRPCUser *Authentication_gRPC.User) (*Authentication_gRPC.User, error) {
	span, ctx := apm.StartSpan(ctx, "EditUser", "gRPC")
	defer span.End()

	userId, err := primitive.ObjectIDFromHex(gRPCUser.Id)
	if err != nil {
		return nil, err
	}

	user := DataStructures.User{
		Id:       userId,
		Email:    gRPCUser.Email,
		Password: gRPCUser.Password,
		IsAdmin:  gRPCUser.IsAdmin,
	}

	err = s.c.EditUser(ctx, user)
	if err != nil {
		return nil, err
	}

	return s.GetUserById(ctx, &Authentication_gRPC.StringData{Data: user.Id.Hex()})
}

func (s *gRPCServer) DeleteUser(ctx context.Context, data *Authentication_gRPC.StringData) (*Authentication_gRPC.StringData, error) {
	span, ctx := apm.StartSpan(ctx, "DeleteUser", "gRPC")
	defer span.End()

	userId, err := primitive.ObjectIDFromHex(data.Data)
	if err != nil {
		return nil, err
	}

	err = s.c.RemoveUser(ctx, userId)

	return &Authentication_gRPC.StringData{Data: "Deleted user"}, err
}
