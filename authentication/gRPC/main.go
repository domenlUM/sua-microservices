package gRPC

import (
	"context"
	"fmt"
	"net"
	"net/http"

	"github.com/improbable-eng/grpc-web/go/grpcweb"

	"github.com/getsentry/sentry-go"
	"github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/labstack/gommon/log"
	"github.com/streadway/amqp"
	"gitlab.com/domenlUM/sua-microservices/Generated/Authentication_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/authentication/Logic"
	"gitlab.com/domenlUM/sua-microservices/grpcMiddleware"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmgrpc"
	"google.golang.org/grpc"
)

type gRPCServer struct {
	c *Logic.Controller
	Authentication_gRPC.UnimplementedAuthenticationAPIServer
}

func New(c *Logic.Controller) *gRPCServer {
	return &gRPCServer{c: c}
}

func (s *gRPCServer) Start(stopChannel chan bool, amqpCh *amqp.Channel, queue *amqp.Queue) {
	options := []grpc.ServerOption{
		grpc.ChainUnaryInterceptor(grpc_recovery.UnaryServerInterceptor(), apmgrpc.NewUnaryServerInterceptor(), grpcMiddleware.CorrelationIdLogger(amqpCh, queue, "AUTHENTICATION")),
		grpc.ChainStreamInterceptor(grpc_recovery.StreamServerInterceptor(), apmgrpc.NewStreamServerInterceptor()),
	}

	grpcServer := grpc.NewServer(options...)

	wrappedGrpc := grpcweb.WrapServer(grpcServer, grpcweb.WithOriginFunc(func(origin string) bool {
		return true
	}))

	go func(stopChannel chan bool) {
		<-stopChannel

		grpcServer.GracefulStop()
	}(stopChannel)

	go func() {
		lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8001))
		if err != nil {
			log.Error("server listen on port %d error:%v", 8001, err)
			return
		}

		Authentication_gRPC.RegisterAuthenticationAPIServer(grpcServer, s)
		fmt.Println("Starting gRPC")
		if err := grpcServer.Serve(lis); err != nil && err != http.ErrServerClosed {
			sentry.CaptureException(err)
		}
	}()

	go func() {
		lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8002))
		if err != nil {
			log.Error("server listen on port %d error:%v", 8002, err)
			return
		}

		grpcHandler := http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
			wrappedGrpc.ServeHTTP(resp, req)
		})

		_ = http.Serve(lis, grpcHandler)
	}()
}

func (s *gRPCServer) Ping(ctx context.Context, _ *Authentication_gRPC.Empty) (*Common_gRPC.Status, error) {
	span, ctx := apm.StartSpan(ctx, "Ping", "gRPC")
	defer span.End()

	return &Common_gRPC.Status{
		Online: true,
		Title:  "Authentication",
	}, nil
}
