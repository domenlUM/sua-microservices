package Logic

import "gitlab.com/domenlUM/sua-microservices/authentication/DB"

type Controller struct {
	db        DB.DB
	jwtSecret []byte
}

func New(db DB.DB, jwtSecret []byte) *Controller {
	return &Controller{
		db:        db,
		jwtSecret: jwtSecret,
	}
}
