package Logic

import (
	"context"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

func (c *Controller) GetUsers(ctx context.Context) (users []DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUsers", "Logic")
	defer span.End()

	return c.db.GetUsers(ctx)
}

func (c *Controller) GetUserById(ctx context.Context, userId primitive.ObjectID) (user DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUserById", "Logic")
	defer span.End()

	return c.db.GetUserById(ctx, userId)
}

func (c *Controller) GetUserByEmail(ctx context.Context, email string) (user DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUserByEmail", "Logic")
	defer span.End()

	return c.db.GetUserByEmail(ctx, email)
}

func (c *Controller) AddUser(ctx context.Context, user DataStructures.User) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "AddUser", "Logic")
	defer span.End()

	return c.db.AddUser(ctx, user)
}

func (c *Controller) EditUser(ctx context.Context, user DataStructures.User) (err error) {
	span, ctx := apm.StartSpan(ctx, "EditUser", "Logic")
	defer span.End()

	return c.db.EditUser(ctx, user)
}

func (c *Controller) RemoveUser(ctx context.Context, userId primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "RemoveUser", "Logic")
	defer span.End()

	return c.db.RemoveUser(ctx, userId)
}

func (c *Controller) Login(ctx context.Context, loginInfo DataStructures.UserLogin) (token string, err error) {
	span, ctx := apm.StartSpan(ctx, "Login", "Logic")
	defer span.End()

	user, err := c.db.GetUserByEmail(ctx, loginInfo.Email)
	if err != nil {
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(loginInfo.Password))
	if err != nil {
		return
	}

	token, err = c.generateTokenForUser(ctx, user.Id)

	return
}

func (c *Controller) Register(ctx context.Context, loginInfo DataStructures.UserLogin) (token string, err error) {
	span, ctx := apm.StartSpan(ctx, "Register", "Logic")
	defer span.End()

	hashed, err := bcrypt.GenerateFromPassword([]byte(loginInfo.Password), bcrypt.DefaultCost)
	if err != nil {
		return
	}

	id, err := c.db.AddUser(ctx, DataStructures.User{Email: loginInfo.Email, Password: string(hashed), IsAdmin: false})
	if err != nil {
		return "", err
	}

	token, err = c.generateTokenForUser(ctx, id)

	return
}

func (c *Controller) generateTokenForUser(ctx context.Context, userId primitive.ObjectID) (tokenString string, err error) {
	span, ctx := apm.StartSpan(ctx, "generateTokenForUser", "Logic")
	defer span.End()

	user, err := c.GetUserById(ctx, userId)
	if err != nil {
		return
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp":  time.Now().Add(60 * time.Hour).Unix(),
		"iat":  time.Now().Unix(),
		"sub":  user.Id.Hex(),
		"name": user.Email,
	})

	tokenString, err = token.SignedString(c.jwtSecret)

	return
}
