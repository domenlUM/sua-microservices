package Logic

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c *Controller) GetServices(ctx context.Context) (services []DataStructures.Service, err error) {
	span, ctx := apm.StartSpan(ctx, "GetServices", "Logic")
	defer span.End()

	return c.db.GetServices(ctx)
}

func (c *Controller) GetServiceById(ctx context.Context, serviceID primitive.ObjectID) (service DataStructures.Service, err error) {
	span, ctx := apm.StartSpan(ctx, "GetServiceById", "Logic")
	defer span.End()

	return c.db.GetServiceById(ctx, serviceID)
}

func (c *Controller) ChangeServicePrice(ctx context.Context, service DataStructures.Service) (err error) {
	span, ctx := apm.StartSpan(ctx, "ChangeServicePrice", "Logic")
	defer span.End()

	return c.db.ChangeServicePrice(ctx, service)
}
