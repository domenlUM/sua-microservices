package Logic

import "gitlab.com/domenlUM/sua-microservices/service/DB"

type Controller struct {
	db DB.DB
}

func New(db DB.DB) *Controller {
	return &Controller{
		db: db,
	}
}
