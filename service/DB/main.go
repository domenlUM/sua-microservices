package DB

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DB interface {
	Init(ctx context.Context) (err error)
	Close(ctx context.Context) (err error)
	GetServiceById(ctx context.Context, serviceID primitive.ObjectID) (service DataStructures.Service, err error)
	GetServices(ctx context.Context) (service []DataStructures.Service, err error)
	ChangeServicePrice(ctx context.Context, service DataStructures.Service) (err error)
}
