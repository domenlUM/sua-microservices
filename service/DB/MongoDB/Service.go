package MongoDB

import (
	"context"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"go.elastic.co/apm"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (db *MongoDB) GetServices(ctx context.Context) (services []DataStructures.Service, err error) {
	span, ctx := apm.StartSpan(ctx, "GetServices", "MongoDB")
	defer span.End()

	cursor, err := db.client.Database(db.database).Collection("services").Find(ctx, bson.M{})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	services = make([]DataStructures.Service, 0)

	err = cursor.All(ctx, &services)
	if err != nil {
		sentry.CaptureException(err)
		return
	}
	return
}

func (db *MongoDB) GetServiceById(ctx context.Context, serviceID primitive.ObjectID) (service DataStructures.Service, err error) {
	span, ctx := apm.StartSpan(ctx, "GetServiceById", "MongoDB")
	defer span.End()

	err = db.client.Database(db.database).Collection("services").FindOne(ctx, bson.M{"_id": serviceID}).Decode(&service)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}

func (db *MongoDB) ChangeServicePrice(ctx context.Context, service DataStructures.Service) (err error) {
	span, ctx := apm.StartSpan(ctx, "ChangeServicePrice", "MongoDB")
	defer span.End()

	_, err = db.client.Database(db.database).Collection("tickets").UpdateOne(ctx, bson.M{"_id": service.Id}, bson.M{
		"$set": bson.M{"Price": service.Price},
	})
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	return
}
