package main

import (
	"context"
	"fmt"
	"os"

	"github.com/streadway/amqp"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/service/DB/MongoDB"
	"gitlab.com/domenlUM/sua-microservices/service/Logic"
	"gitlab.com/domenlUM/sua-microservices/service/gRPC"
)

func main() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://46555a8da0524472bed3b56799fe6061@o1035781.ingest.sentry.io/6147810",
	})
	if err != nil {
		fmt.Printf("sentry.Init: %s\n", err)
	}

	ctx := context.Background()

	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		sentry.CaptureException(err)
	}

	ch, err := conn.Channel()
	if err != nil {
		sentry.CaptureException(err)
	}

	q, err := ch.QueueDeclare(
		"logging", // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		sentry.CaptureException(err)
	}

	mongoDB := MongoDB.New(
		getEnv("MONGO_USER", "root"),
		getEnv("MONGO_PASS", "root"),
		getEnv("MONGO_IP", "mongo-service"),
		getEnv("MONGO_PORT", "27017"),
		getEnv("MONGO_DB", "service"),
		getEnv("MONGO_AUTH_DB", "admin"),
		getEnv("MONGO_AUTH_MECHANISM", "SCRAM-SHA-256"))
	err = mongoDB.Init(ctx)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	logicController := Logic.New(mongoDB)
	if err != nil {
		sentry.CaptureException(err)
		fmt.Println(err.Error())
		return
	}

	grpcAPI := gRPC.New(logicController)

	signalChan := make(chan os.Signal, 0)
	closeGRPC := make(chan bool, 0)

	grpcAPI.Start(closeGRPC, ch, &q)

	<-signalChan

	close(closeGRPC)
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
