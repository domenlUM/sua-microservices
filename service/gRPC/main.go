package gRPC

import (
	"context"
	"fmt"
	"net"
	"net/http"

	"gitlab.com/domenlUM/sua-microservices/grpcMiddleware"

	"github.com/streadway/amqp"

	"gitlab.com/domenlUM/sua-microservices/Generated/Service_gRPC"

	"github.com/getsentry/sentry-go"
	"github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/labstack/gommon/log"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/service/Logic"
	"go.elastic.co/apm/module/apmgrpc"
	"google.golang.org/grpc"
)

type gRPCServer struct {
	c *Logic.Controller
	Service_gRPC.UnimplementedServiceAPIServer
}

func New(c *Logic.Controller) *gRPCServer {
	return &gRPCServer{c: c}
}

func (s *gRPCServer) Start(stopChannel chan bool, amqpCh *amqp.Channel, queue *amqp.Queue) {
	options := []grpc.ServerOption{
		grpc.ChainUnaryInterceptor(grpc_recovery.UnaryServerInterceptor(), apmgrpc.NewUnaryServerInterceptor(), grpcMiddleware.CorrelationIdLogger(amqpCh, queue, "SERVICE")),
		grpc.ChainStreamInterceptor(grpc_recovery.StreamServerInterceptor(), apmgrpc.NewStreamServerInterceptor()),
	}

	grpcServer := grpc.NewServer(options...)

	go func(stopChannel chan bool) {
		<-stopChannel

		grpcServer.GracefulStop()
	}(stopChannel)

	go func() {
		lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8001))
		if err != nil {
			log.Error("server listen on port %d error:%v", 8001, err)
			return
		}

		Service_gRPC.RegisterServiceAPIServer(grpcServer, s)
		fmt.Println("Starting gRPC")
		if err := grpcServer.Serve(lis); err != nil && err != http.ErrServerClosed {
			sentry.CaptureException(err)
		}
	}()
}

func (s *gRPCServer) Ping(_ context.Context, _ *Common_gRPC.Empty) (*Common_gRPC.Status, error) {
	return &Common_gRPC.Status{
		Online: true,
		Title:  "Service",
	}, nil
}
