package gRPC

import (
	"context"

	"gitlab.com/domenlUM/sua-microservices/DataStructures"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Service_gRPC"
)

func (s *gRPCServer) GetStatusForService(_ context.Context, _ *Common_gRPC.StringData) (*Common_gRPC.Status, error) {
	zdravje := new(Common_gRPC.Status)
	zdravje.Online = true

	return zdravje, nil
}

func (s *gRPCServer) GetServices(ctx context.Context, _ *Common_gRPC.Empty) (*Service_gRPC.Services, error) {
	services, err := s.c.GetServices(ctx)
	if err != nil {
		return nil, err
	}

	gRPCServices := make([]*Service_gRPC.Service, len(services))
	for i := range services {
		gRPCServices[i] = &Service_gRPC.Service{
			Id:          services[i].Id.Hex(),
			Purpose:     services[i].Purpose,
			Description: services[i].Description,
			Price:       services[i].Price,
			Status:      &Common_gRPC.Status{Online: true},
		}
	}

	return &Service_gRPC.Services{Services: gRPCServices}, err
}

func (s *gRPCServer) GetServiceById(ctx context.Context, data *Common_gRPC.StringData) (servicegrpc *Service_gRPC.Service, err error) {
	serviceId, err := primitive.ObjectIDFromHex(data.Data)
	if err != nil {
		return
	}
	service, err := s.c.GetServiceById(ctx, serviceId)
	if err != nil {
		return
	}
	servicegrpc = &Service_gRPC.Service{
		Id:          service.Id.Hex(),
		Purpose:     service.Purpose,
		Description: service.Description,
		Price:       service.Price,
		Status:      &Common_gRPC.Status{Online: service.Status.Status},
	}
	return
}

func (s *gRPCServer) ChangeServicePrice(ctx context.Context, request *Service_gRPC.ChangeServicePriceRequest) (serviceGrpc *Service_gRPC.Service, err error) {
	serviceId, err := primitive.ObjectIDFromHex(request.Id)
	if err != nil {
		return
	}
	service := DataStructures.Service{Id: serviceId, Price: float64(request.NewPrice)}
	if err != nil {
		return
	}
	err = s.c.ChangeServicePrice(ctx, service)
	if err != nil {
		return
	}

	return s.GetServiceById(ctx, &Common_gRPC.StringData{Data: request.Id})
}
