package PaymentsAPI

import (
	"context"
	"errors"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Payment_gRPC"
	"gitlab.com/domenlUM/sua-microservices/grpcMiddleware"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmgrpc"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
)

type Client struct {
	client     Payment_gRPC.PaymentAPIClient
	grpcClient *grpc.ClientConn
}

func New() *Client {
	return &Client{}
}

func (s *Client) Init(ctx context.Context, address string) (err error) {
	span, ctx := apm.StartSpan(ctx, "Init", "gRPC_Client")
	defer span.End()

	conn, err := grpc.Dial(address,
		grpc.WithInsecure(),
		grpc.WithStreamInterceptor(apmgrpc.NewStreamClientInterceptor()),
		grpc.WithChainUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor(), grpcMiddleware.AddCorrelationFromContext()))
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	s.client = Payment_gRPC.NewPaymentAPIClient(conn)
	response, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	if !response.Online {
		err = errors.New("error during grpc ping")
	}
	return
}

func (s *Client) Close() error {
	return s.grpcClient.Close()
}

func (s *Client) Ping(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "Ping", "gRPC_Client")
	defer span.End()

	statusGrpc, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	zdravje = DataStructures.Status{
		Status:  statusGrpc.Online,
		Service: statusGrpc.Title,
	}
	return
}

func (s *Client) GetPayments(ctx context.Context) (payments []DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "GetPayments", "gRPC_Client")
	defer span.End()

	paymentsGrpc, err := s.client.GetPayments(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	payments = make([]DataStructures.Payment, len(paymentsGrpc.Payments))
	for i := range paymentsGrpc.Payments {
		paymentId, err1 := primitive.ObjectIDFromHex(paymentsGrpc.Payments[i].Id)
		if err1 != nil {
			err = err1
			return
		}
		payments[i] = DataStructures.Payment{
			Id:           paymentId,
			Purpose:      paymentsGrpc.Payments[i].Purpose,
			Price:        paymentsGrpc.Payments[i].Price,
			ForOrder:     paymentsGrpc.Payments[i].ForOrder,
			PaymentState: int(paymentsGrpc.Payments[i].PaymentState),
		}
	}

	return
}

func (s *Client) GetPaymentById(ctx context.Context, paymentId string) (payment DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "GetPaymentById", "gRPC_Client")
	defer span.End()

	paymentGrpc, err := s.client.GetPaymentById(ctx, &Common_gRPC.StringData{Data: paymentId})
	if err != nil {
		return
	}

	paymentOId, err1 := primitive.ObjectIDFromHex(paymentGrpc.Id)
	if err1 != nil {
		err = err1
		return
	}
	payment = DataStructures.Payment{
		Id:           paymentOId,
		Purpose:      paymentGrpc.Purpose,
		Price:        paymentGrpc.Price,
		ForOrder:     paymentGrpc.ForOrder,
		PaymentState: int(paymentGrpc.PaymentState),
	}

	return
}

func (s *Client) ClosePayment(ctx context.Context, paymentId string) (err error) {
	span, ctx := apm.StartSpan(ctx, "ClosePayment", "gRPC_Client")
	defer span.End()

	_, err = s.client.ClosePayment(ctx, &Common_gRPC.StringData{Data: paymentId})

	return
}

func (s *Client) OpenPayment(ctx context.Context, payment DataStructures.Payment) (paymentNew DataStructures.Payment, err error) {
	span, ctx := apm.StartSpan(ctx, "OpenPayment", "gRPC_Client")
	defer span.End()

	paymentGrpc := &Payment_gRPC.Payment{
		Id:           payment.Id.Hex(),
		Purpose:      payment.Purpose,
		Price:        payment.Price,
		ForOrder:     payment.ForOrder,
		PaymentState: int64(payment.PaymentState),
	}

	newPaymentGRPC, err := s.client.OpenPayment(ctx, paymentGrpc)
	if err != nil {
		return
	}
	newPaymentId, err := primitive.ObjectIDFromHex(newPaymentGRPC.Id)
	if err != nil {
		return
	}

	paymentNew = DataStructures.Payment{
		Id:           newPaymentId,
		Purpose:      newPaymentGRPC.Purpose,
		Price:        newPaymentGRPC.Price,
		ForOrder:     newPaymentGRPC.ForOrder,
		PaymentState: int(newPaymentGRPC.PaymentState),
	}
	return
}
