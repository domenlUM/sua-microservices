package ServiceAPI

import (
	"context"
	"errors"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Service_gRPC"
	"gitlab.com/domenlUM/sua-microservices/grpcMiddleware"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmgrpc"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
)

type Client struct {
	client     Service_gRPC.ServiceAPIClient
	grpcClient *grpc.ClientConn
}

func New() *Client {
	return &Client{}
}

func (s *Client) Init(ctx context.Context, address string) (err error) {
	span, ctx := apm.StartSpan(ctx, "Init", "gRPC_Client")
	defer span.End()

	conn, err := grpc.Dial(address,
		grpc.WithInsecure(),
		grpc.WithStreamInterceptor(apmgrpc.NewStreamClientInterceptor()),
		grpc.WithChainUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor(), grpcMiddleware.AddCorrelationFromContext()))
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	s.client = Service_gRPC.NewServiceAPIClient(conn)
	response, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	if !response.Online {
		err = errors.New("error during grpc ping")
	}
	return
}

func (s *Client) Ping(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "Ping", "gRPC_Client")
	defer span.End()

	statusGrpc, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	zdravje = DataStructures.Status{
		Status:  statusGrpc.Online,
		Service: statusGrpc.Title,
	}
	return
}

func (s *Client) Close() error {
	return s.grpcClient.Close()
}

func (s *Client) GetStatusForService(ctx context.Context, serviceId string) (status DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "GetStatusForService", "gRPC_Client")
	defer span.End()
	statusGrpc, err := s.client.GetStatusForService(ctx, &Common_gRPC.StringData{Data: serviceId})
	if err != nil {
		return
	}

	status = DataStructures.Status{Status: statusGrpc.Online}

	return
}

func (s *Client) GetServices(ctx context.Context) (services []DataStructures.Service, err error) {
	span, ctx := apm.StartSpan(ctx, "GetServices", "gRPC_Client")
	defer span.End()

	servicesGrpc, err := s.client.GetServices(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}

	services = make([]DataStructures.Service, len(servicesGrpc.Services))
	for i := range servicesGrpc.Services {
		serviceId, err1 := primitive.ObjectIDFromHex(servicesGrpc.Services[i].Id)
		if err1 != nil {
			err = err1
			return
		}
		services[i] = DataStructures.Service{
			Id:          serviceId,
			Purpose:     servicesGrpc.Services[i].Purpose,
			Description: servicesGrpc.Services[i].Description,
			Price:       servicesGrpc.Services[i].Price,
			Status:      DataStructures.Status{Status: servicesGrpc.Services[i].Status.Online},
		}
	}
	return
}

func (s *Client) GetServiceById(ctx context.Context, serviceId string) (service DataStructures.Service, err error) {
	span, ctx := apm.StartSpan(ctx, "GetServiceById", "gRPC_Client")
	defer span.End()

	serviceGrpc, err := s.client.GetServiceById(ctx, &Common_gRPC.StringData{Data: serviceId})
	if err != nil {
		return
	}

	serviceOId, err := primitive.ObjectIDFromHex(serviceGrpc.Id)
	if err != nil {
		return
	}

	service = DataStructures.Service{
		Id:          serviceOId,
		Purpose:     serviceGrpc.Purpose,
		Description: serviceGrpc.Description,
		Price:       serviceGrpc.Price,
		Status:      DataStructures.Status{Status: serviceGrpc.Status.Online},
	}
	return
}

func (s *Client) ChangeServicePrice(ctx context.Context, serviceId string, newPrice float64) (err error) {
	span, ctx := apm.StartSpan(ctx, "ChangeServicePrice", "gRPC_Client")
	defer span.End()

	_, err = s.client.ChangeServicePrice(ctx, &Service_gRPC.ChangeServicePriceRequest{
		Id:       serviceId,
		NewPrice: float32(newPrice),
	})

	return
}
