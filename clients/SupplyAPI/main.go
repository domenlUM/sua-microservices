package SupplyAPI

import (
	"context"
	"errors"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Supply_gRPC"
	"gitlab.com/domenlUM/sua-microservices/grpcMiddleware"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmgrpc"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
)

type Client struct {
	client     Supply_gRPC.SupplyAPIClient
	grpcClient *grpc.ClientConn
}

func New() *Client {
	return &Client{}
}

func (s *Client) Init(ctx context.Context, address string) (err error) {
	conn, err := grpc.Dial(address,
		grpc.WithInsecure(),
		grpc.WithStreamInterceptor(apmgrpc.NewStreamClientInterceptor()),
		grpc.WithChainUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor(), grpcMiddleware.AddCorrelationFromContext()))
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	s.client = Supply_gRPC.NewSupplyAPIClient(conn)
	response, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	if !response.Online {
		err = errors.New("error during grpc ping")
	}
	return
}

func (s *Client) Close() error {
	return s.grpcClient.Close()
}

func (s *Client) GetOrders(ctx context.Context) (orders []DataStructures.Order, err error) {
	span, ctx := apm.StartSpan(ctx, "GetOrders", "gRPC_Client")
	defer span.End()

	ordersGrpc, err := s.client.GetOrders(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	orders = make([]DataStructures.Order, len(ordersGrpc.Orders))
	for i := range ordersGrpc.Orders {
		orderOId, err1 := primitive.ObjectIDFromHex(ordersGrpc.Orders[i].OrderId)
		if err1 != nil {
			err = err1
			return
		}
		items := make([]DataStructures.Item, len(ordersGrpc.Orders[i].OrderedItems.Items))
		for j := range ordersGrpc.Orders[i].OrderedItems.Items {
			itemOid, err1 := primitive.ObjectIDFromHex(ordersGrpc.Orders[i].OrderedItems.Items[j].ItemId)
			if err1 != nil {
				err = err1
				return
			}
			items[j] = DataStructures.Item{
				ItemId:  itemOid,
				Name:    ordersGrpc.Orders[i].OrderedItems.Items[j].Name,
				Model:   ordersGrpc.Orders[i].OrderedItems.Items[j].Model,
				InStock: ordersGrpc.Orders[i].OrderedItems.Items[j].InStock,
			}
		}
		orders[i] = DataStructures.Order{
			OrderId:      orderOId,
			OrderedBy:    ordersGrpc.Orders[i].OrderedBy,
			Price:        ordersGrpc.Orders[i].Price,
			OrderedItems: items,
			OrderedFrom:  ordersGrpc.Orders[i].OrderedFrom.SupplierId,
		}
	}

	return
}

func (s *Client) GetOrderById(ctx context.Context, orderId string) (order DataStructures.Order, err error) {
	span, ctx := apm.StartSpan(ctx, "GetOrderById", "gRPC_Client")
	defer span.End()

	orderGrpc, err := s.client.GetOrderById(ctx, &Common_gRPC.StringData{Data: orderId})
	if err != nil {
		return
	}

	orderOId, err1 := primitive.ObjectIDFromHex(orderGrpc.OrderId)
	if err1 != nil {
		err = err1
		return
	}
	items := make([]DataStructures.Item, len(orderGrpc.OrderedItems.Items))
	for j := range orderGrpc.OrderedItems.Items {
		itemOid, err1 := primitive.ObjectIDFromHex(orderGrpc.OrderedItems.Items[j].ItemId)
		if err1 != nil {
			err = err1
			return
		}
		items[j] = DataStructures.Item{
			ItemId:  itemOid,
			Name:    orderGrpc.OrderedItems.Items[j].Name,
			Model:   orderGrpc.OrderedItems.Items[j].Model,
			InStock: orderGrpc.OrderedItems.Items[j].InStock,
		}
	}

	order = DataStructures.Order{
		OrderId:      orderOId,
		OrderedBy:    orderGrpc.OrderedBy,
		Price:        orderGrpc.Price,
		OrderedItems: items,
		OrderedFrom:  orderGrpc.OrderedFrom.SupplierId,
	}

	return
}

func (s *Client) CreateOrder(ctx context.Context, order DataStructures.Order) (err error) {
	span, ctx := apm.StartSpan(ctx, "CreateOrder", "gRPC_Client")
	defer span.End()

	_, err = s.client.CreateOrder(ctx, &Common_gRPC.Order{
		OrderId:   "",
		OrderedBy: order.OrderedBy,
		Price:     order.Price,
		// OrderedItems:     order.OrderedItems,
		// OrderedFrom: order.OrderedFrom,
	})

	return
}

func (s *Client) CancelOrder(ctx context.Context, orderId string) (err error) {
	span, ctx := apm.StartSpan(ctx, "CancelOrder", "gRPC_Client")
	defer span.End()

	_, err = s.client.CancelOrder(ctx, &Common_gRPC.StringData{Data: orderId})

	return
}

func (s *Client) GetItems(ctx context.Context) (items []DataStructures.Item, err error) {
	span, ctx := apm.StartSpan(ctx, "GetItems", "gRPC_Client")
	defer span.End()

	itemsGrpc, err := s.client.GetItems(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	items = make([]DataStructures.Item, len(itemsGrpc.Items))
	for i := range itemsGrpc.Items {
		itemId, err1 := primitive.ObjectIDFromHex(itemsGrpc.Items[i].ItemId)
		if err1 != nil {
			err = err1
			return
		}
		items[i] = DataStructures.Item{
			ItemId:  itemId,
			Name:    itemsGrpc.Items[i].Name,
			Model:   itemsGrpc.Items[i].Model,
			InStock: itemsGrpc.Items[i].InStock,
		}
	}

	return
}

func (s *Client) GetItemById(ctx context.Context, itemId string) (item DataStructures.Item, err error) {
	span, ctx := apm.StartSpan(ctx, "GetItemById", "gRPC_Client")
	defer span.End()

	itemGrpc, err := s.client.GetItemById(ctx, &Common_gRPC.StringData{Data: itemId})
	if err != nil {
		return
	}

	itemOId, err1 := primitive.ObjectIDFromHex(itemGrpc.ItemId)
	if err1 != nil {
		err = err1
		return
	}
	item = DataStructures.Item{
		ItemId:  itemOId,
		Name:    itemGrpc.Name,
		Model:   itemGrpc.Model,
		InStock: itemGrpc.InStock,
	}

	return
}

func (s *Client) Ping(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "Ping", "gRPC_Client")
	defer span.End()

	statusGrpc, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	zdravje = DataStructures.Status{
		Status:  statusGrpc.Online,
		Service: statusGrpc.Title,
	}
	return
}

func (s *Client) GetSuppliers(ctx context.Context) (suppliers []DataStructures.Supplier, err error) {
	span, ctx := apm.StartSpan(ctx, "GetSuppliers", "gRPC_Client")
	defer span.End()

	suppliersGrpc, err := s.client.GetSuppliers(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}

	suppliers = make([]DataStructures.Supplier, len(suppliersGrpc.Suppliers))
	for i := range suppliersGrpc.Suppliers {
		supplierOId, err1 := primitive.ObjectIDFromHex(suppliersGrpc.Suppliers[i].SupplierId)
		if err1 != nil {
			err = err1
			return
		}
		suppliers[i] = DataStructures.Supplier{
			SupplierId:  supplierOId,
			Name:        suppliersGrpc.Suppliers[i].Name,
			Address:     suppliersGrpc.Suppliers[i].Address,
			PhoneNumber: suppliersGrpc.Suppliers[i].PhoneNumber,
			Email:       suppliersGrpc.Suppliers[i].Email,
		}
	}

	return
}

func (s *Client) GetSupplierById(ctx context.Context, supplierId string) (supplier DataStructures.Supplier, err error) {
	span, ctx := apm.StartSpan(ctx, "GetSupplierById", "gRPC_Client")
	defer span.End()

	suppliersGrpc, err := s.client.GetSupplierById(ctx, &Common_gRPC.StringData{Data: supplierId})
	if err != nil {
		return
	}

	supplierOId, err1 := primitive.ObjectIDFromHex(suppliersGrpc.SupplierId)
	if err1 != nil {
		err = err1
		return
	}
	supplier = DataStructures.Supplier{
		SupplierId:  supplierOId,
		Name:        suppliersGrpc.Name,
		Address:     suppliersGrpc.Address,
		PhoneNumber: suppliersGrpc.PhoneNumber,
		Email:       suppliersGrpc.Email,
	}

	return
}
