package StatusAPI

import (
	"context"
	"errors"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Status_gRPC"
	"gitlab.com/domenlUM/sua-microservices/grpcMiddleware"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmgrpc"
	"google.golang.org/grpc"
)

type Client struct {
	client     Status_gRPC.StatusAPIClient
	grpcClient *grpc.ClientConn
}

func New() *Client {
	return &Client{}
}

func (s *Client) Ping(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "Ping", "gRPC_Client")
	defer span.End()

	statusGrpc, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	zdravje = DataStructures.Status{
		Status:  statusGrpc.Online,
		Service: statusGrpc.Title,
	}
	return
}

func (s *Client) Init(ctx context.Context, address string) (err error) {
	conn, err := grpc.Dial(address,
		grpc.WithInsecure(),
		grpc.WithStreamInterceptor(apmgrpc.NewStreamClientInterceptor()),
		grpc.WithChainUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor(), grpcMiddleware.AddCorrelationFromContext()))
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	s.client = Status_gRPC.NewStatusAPIClient(conn)
	response, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	if !response.Online {
		err = errors.New("error during grpc ping")
	}
	return
}

func (s *Client) Close() error {
	return s.grpcClient.Close()
}

func (s *Client) PingAll(ctx context.Context) (statuses []DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "PingAll", "gRPC_Client")
	defer span.End()

	statusGrpc, err := s.client.PingAll(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}

	statuses = make([]DataStructures.Status, len(statusGrpc.Statuses))
	for i := range statusGrpc.Statuses {
		statuses[i] = DataStructures.Status{
			Status:  statusGrpc.Statuses[i].Online,
			Service: statusGrpc.Statuses[i].Title,
		}
	}

	return
}
