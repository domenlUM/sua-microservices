package TicketingAPI

import (
	"context"
	"errors"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Ticketing_gRPC"
	"gitlab.com/domenlUM/sua-microservices/grpcMiddleware"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmgrpc"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
)

type Client struct {
	client     Ticketing_gRPC.TicketingAPIClient
	grpcClient *grpc.ClientConn
}

func New() *Client {
	return &Client{}
}

func (s *Client) Ping(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "Ping", "gRPC_Client")
	defer span.End()

	statusGrpc, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	zdravje = DataStructures.Status{
		Status:  statusGrpc.Online,
		Service: statusGrpc.Title,
	}
	return
}

func (s *Client) Init(ctx context.Context, address string) (err error) {
	span, ctx := apm.StartSpan(ctx, "Init", "gRPC_Client")
	defer span.End()

	conn, err := grpc.Dial(address,
		grpc.WithInsecure(),
		grpc.WithStreamInterceptor(apmgrpc.NewStreamClientInterceptor()),
		grpc.WithChainUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor(), grpcMiddleware.AddCorrelationFromContext()))
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	s.client = Ticketing_gRPC.NewTicketingAPIClient(conn)
	response, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	if !response.Online {
		err = errors.New("error during grpc ping")
	}
	return
}

func (s *Client) Close() error {
	return s.grpcClient.Close()
}

func (s *Client) GetStatusForTicketing(ctx context.Context, serviceId string) (status DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "GetStatusForTicketing", "gRPC_Client")
	defer span.End()
	_, err = s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}

	status = DataStructures.Status{Status: true}

	return
}

func (s *Client) GetTickets(ctx context.Context) (tickets []DataStructures.Ticket, err error) {
	span, ctx := apm.StartSpan(ctx, "GetTickets", "gRPC_Client")
	defer span.End()

	ticketsGrpc, err := s.client.GetTickets(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}

	tickets = make([]DataStructures.Ticket, len(ticketsGrpc.Tickets))
	for i := range ticketsGrpc.Tickets {
		ticketId, err1 := primitive.ObjectIDFromHex(ticketsGrpc.Tickets[i].Id)
		if err1 != nil {
			err = err1
			return
		}
		tickets[i] = DataStructures.Ticket{
			Id:               ticketId,
			ShortDescription: ticketsGrpc.Tickets[i].ShortDescription,
			Description:      ticketsGrpc.Tickets[i].Description,
			Date:             ticketsGrpc.Tickets[i].Date,
			TicketState:      int(ticketsGrpc.Tickets[i].TicketState),
		}
	}
	return
}

func (s *Client) GetTicketById(ctx context.Context, ticketId string) (ticket DataStructures.Ticket, err error) {
	span, ctx := apm.StartSpan(ctx, "GetTicketById", "gRPC_Client")
	defer span.End()

	ticketGrpc, err := s.client.GetTicketById(ctx, &Common_gRPC.StringData{Data: ticketId})
	if err != nil {
		return
	}

	ticketOId, err := primitive.ObjectIDFromHex(ticketGrpc.Id)
	if err != nil {
		return
	}

	userId, err := primitive.ObjectIDFromHex(ticketGrpc.SubmittedBy)
	if err != nil {
		return
	}
	ticket = DataStructures.Ticket{
		Id:               ticketOId,
		ShortDescription: ticketGrpc.ShortDescription,
		Description:      ticketGrpc.Description,
		Date:             ticketGrpc.Date,
		TicketState:      int(ticketGrpc.TicketState),
		User:             DataStructures.User{Id: userId},
	}

	return
}

func (s *Client) OpenTicket(ctx context.Context, ticket DataStructures.Ticket) (id string, err error) {
	span, ctx := apm.StartSpan(ctx, "OpenTicket", "gRPC_Client")
	defer span.End()

	ticketGrpc, err := s.client.OpenTicket(ctx, &Ticketing_gRPC.Ticket{
		Id:               "",
		ShortDescription: ticket.ShortDescription,
		SubmittedBy:      ticket.User.Id.Hex(),
		Description:      ticket.Description,
		Date:             ticket.Date,
		TicketState:      int32(ticket.TicketState),
	})

	id = ticketGrpc.Id
	return
}

func (s *Client) CloseTicket(ctx context.Context, ticketId string) (err error) {
	span, ctx := apm.StartSpan(ctx, "CloseTicket", "gRPC_Client")
	defer span.End()

	_, err = s.client.CloseTicket(ctx, &Common_gRPC.StringData{Data: ticketId})

	return
}

func (s *Client) EditTicket(ctx context.Context, ticket DataStructures.Ticket) (err error) {
	span, ctx := apm.StartSpan(ctx, "EditTicket", "gRPC_Client")
	defer span.End()

	_, err = s.client.EditTicket(ctx, &Ticketing_gRPC.Ticket{
		Id:               ticket.Id.Hex(),
		ShortDescription: ticket.ShortDescription,
		SubmittedBy:      ticket.User.Id.Hex(),
		Description:      ticket.Description,
		Date:             ticket.Date,
		TicketState:      int32(ticket.TicketState),
	})

	return
}
