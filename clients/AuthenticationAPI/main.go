package AuthenticationAPI

import (
	"context"
	"errors"

	"github.com/getsentry/sentry-go"
	"gitlab.com/domenlUM/sua-microservices/DataStructures"
	"gitlab.com/domenlUM/sua-microservices/Generated/Authentication_gRPC"
	"gitlab.com/domenlUM/sua-microservices/Generated/Common_gRPC"
	"gitlab.com/domenlUM/sua-microservices/grpcMiddleware"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmgrpc"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
)

type Client struct {
	client     Authentication_gRPC.AuthenticationAPIClient
	grpcClient *grpc.ClientConn
}

func New() *Client {
	return &Client{}
}

func (s *Client) Init(ctx context.Context, address string) (err error) {
	span, ctx := apm.StartSpan(ctx, "EditUser", "gRPC_Client")
	defer span.End()

	conn, err := grpc.Dial(address,
		grpc.WithInsecure(),
		grpc.WithStreamInterceptor(apmgrpc.NewStreamClientInterceptor()),
		grpc.WithChainUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor(), grpcMiddleware.AddCorrelationFromContext()))
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	s.client = Authentication_gRPC.NewAuthenticationAPIClient(conn)

	response, err := s.client.Ping(ctx, &Authentication_gRPC.Empty{})
	if err != nil {
		return
	}
	if !response.Online {
		err = errors.New("error during grpc ping")
	}
	return
}

func (s *Client) Close() error {
	return s.grpcClient.Close()
}

func (s *Client) GetUsers(ctx context.Context) (users []DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUsers", "gRPC_Client")
	defer span.End()

	userGrpc, err := s.client.GetUsers(ctx, &Authentication_gRPC.Empty{})
	if err != nil {
		return nil, err
	}

	users = make([]DataStructures.User, len(userGrpc.Users))
	for i := range userGrpc.Users {
		userId, err1 := primitive.ObjectIDFromHex(userGrpc.Users[i].Id)
		if err1 != nil {
			err = err1
			return
		}
		users[i] = DataStructures.User{
			Id:      userId,
			Email:   userGrpc.Users[i].Email,
			IsAdmin: userGrpc.Users[i].IsAdmin,
		}
	}

	return
}

func (s *Client) GetUserById(ctx context.Context, userId primitive.ObjectID) (user DataStructures.User, err error) {
	span, ctx := apm.StartSpan(ctx, "GetUserById", "gRPC_Client")
	defer span.End()

	userGrpc, err := s.client.GetUserById(ctx, &Authentication_gRPC.StringData{Data: userId.Hex()})
	if err != nil {
		return
	}

	user = DataStructures.User{
		Id:      userId,
		Email:   userGrpc.Email,
		IsAdmin: userGrpc.IsAdmin,
	}
	return
}

func (s *Client) AddUser(ctx context.Context, user DataStructures.User) (id primitive.ObjectID, err error) {
	span, ctx := apm.StartSpan(ctx, "AddUser", "gRPC_Client")
	defer span.End()

	newUser, err := s.client.AddUser(ctx, &Authentication_gRPC.User{
		Id:       user.Id.Hex(),
		Email:    user.Email,
		Password: user.Password,
		IsAdmin:  user.IsAdmin,
	})
	if err != nil {
		return
	}

	return primitive.ObjectIDFromHex(newUser.Id)
}

func (s *Client) EditUser(ctx context.Context, user DataStructures.User) (err error) {
	span, ctx := apm.StartSpan(ctx, "EditUser", "gRPC_Client")
	defer span.End()

	_, err = s.client.EditUser(ctx, &Authentication_gRPC.User{
		Id:       user.Id.Hex(),
		Email:    user.Email,
		Password: user.Password,
		IsAdmin:  user.IsAdmin,
	})

	return
}

func (s *Client) RemoveUser(ctx context.Context, userId primitive.ObjectID) (err error) {
	span, ctx := apm.StartSpan(ctx, "RemoveUser", "gRPC_Client")
	defer span.End()

	_, err = s.client.DeleteUser(ctx, &Authentication_gRPC.StringData{Data: userId.Hex()})
	return
}

func (s *Client) Login(ctx context.Context, userLogin DataStructures.UserLogin) (token string, err error) {
	span, ctx := apm.StartSpan(ctx, "Login", "gRPC_Client")
	defer span.End()

	grpcToken, err := s.client.Login(ctx, &Authentication_gRPC.UserLogin{
		Email:    userLogin.Email,
		Password: userLogin.Password,
	})
	if err != nil {
		return "", err
	}

	return grpcToken.Token, nil
}

func (s *Client) Register(ctx context.Context, userLogin DataStructures.UserLogin) (token string, err error) {
	span, ctx := apm.StartSpan(ctx, "Register", "gRPC_Client")
	defer span.End()
	grpcToken, err := s.client.Register(ctx, &Authentication_gRPC.UserLogin{
		Email:    userLogin.Email,
		Password: userLogin.Password,
	})
	if err != nil {
		return "", err
	}

	return grpcToken.Token, nil
}

func (s *Client) Ping(ctx context.Context) (zdravje DataStructures.Status, err error) {
	span, ctx := apm.StartSpan(ctx, "Ping", "gRPC_Client")
	defer span.End()

	statusGrpc, err := s.client.Ping(ctx, &Common_gRPC.Empty{})
	if err != nil {
		return
	}
	zdravje = DataStructures.Status{
		Status:  statusGrpc.Online,
		Service: statusGrpc.Title,
	}
	return
}
